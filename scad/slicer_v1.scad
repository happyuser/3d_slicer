
names = ["torus.stl","moskow_city.STL","cone.stl","cube.stl","monkey.stl","r4_2.stl"];
modules = ["module.stl"];

user_file=names[0];   // must be initalised
user_filename=user_file; // param1 passed via -D on cmd-line
echo("cutting user file:",user_file);

p_model_size_x = 0;
model_size_x = p_model_size_x;

p_slice_vertice = "x";//or y
slice_vertice  = p_slice_vertice; //param from console

slice1=0;   // must be initalised
p_slice1=user_file; // param1 passed via -D on 

include <TOUL.scad>;
//include <strings.scad>;
p_s_slice_size = "0,150,300,600,800"; //cm
s_slice_size  = p_s_slice_size;

p = "1"; //cm
p_n  = p;

u_min_x = -50;
u_max_x = 50;

u_s_x = u_max_x -  u_min_x ;

cube_size = 120;


a_slice_size = split(s_slice_size , ",");
echo("Got slices points array:", a_slice_size);
echo("Geting slice #", p_n);
//slice s_slice_size  by ','
function strToNbr(str, i=0, nb=0) = i == len(str) ? nb : nb+strToNbr(str, i+1, search(str[i],"0123456789")[0]*pow(10,len(str)-i-1));


module u_object()
{
    resize([100,0,0], auto=true) //scales to 100 x size.
        //color([0.5,0,0,0.2]) 
    import(user_filename, convexity=3, center=true);
    echo("2. got user file! ", user_filename);
    
}

slices_c =  len(a_slice_size);
sum = 0;
for(x = [0:1:slices_c-1]) { 
	a = atoi(a_slice_size[x]);
    //echo(a);
	sum = sum + a;
    //echo("sum ", sum );
}


//echo("sum",sum);
//for(x = [0:1:0]) {//slices_c-1
//echo(a_slice_size[x]);
part_border_max = 0;
x = atoi(p_n)-1;//0;

sum_x = 0;
//a = 0;
for(i = [0:1:x]) { 
	a = atoi(a_slice_size[i]);
    //echo(a);
	sum_x = (sum_x + a);
}

//echo("sum_x ", sum_x );

    x_0 = u_min_x - cube_size + atoi(a_slice_size[x])/1000*u_s_x; 
    x_1 = u_min_x + atoi(a_slice_size[x+1])/1000*u_s_x; 
echo("slice start %x0:", atoi(a_slice_size[x])/1000);
echo("slice end %x1", atoi(a_slice_size[x+1])/1000);
difference(){
difference(){
color([0.5,0.5,0,0.2]) u_object();
    
color([0,0.5,0.5,0.2]) translate([x_0,-cube_size/2,-cube_size/2])cube(size = cube_size, center = false);
}
color([0,0.5,0.5,0.2]) translate([x_1,-cube_size/2,-cube_size/2])cube(size = cube_size, center = false);
}
echo("Done.");
echo(str("se result in: ../scad_output/r",p_n,".stl"));
