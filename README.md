The 3D slicer application may be used for cutting the 3D model.
The supported format of 3D model is stl.

The 3D slicer was developed by C++ language.

The used technologies are:

* Qt 5 library.

* OpenGL subsystem of Qt library for displaying the 3D model preview.

* Multithreading.

* Qt creator as IDE for developing the application.

* The openscad software for executing the scad script which cuts the 3D model.

* The admesh application for getting the information about the 3D model.

Demo video: http://vps1.happyuser.info/3d_slicer_demo.ogv

Openscad developer: Taras Prystavskiy

C++ qt developer: Sergiy Vovk

HappyuseS team. Visit http://happyuser.info