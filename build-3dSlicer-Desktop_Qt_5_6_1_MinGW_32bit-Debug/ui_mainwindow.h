/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *bannerPicture;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *modelFilePathEdit;
    QPushButton *browseButton;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer;
    QLabel *label_3;
    QLineEdit *modelSizeXEdit;
    QLabel *label_4;
    QLineEdit *modelSizeYEdit;
    QLabel *label_6;
    QLineEdit *modelSizeZEdit;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_9;
    QComboBox *cutAxisCombo;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_10;
    QLineEdit *numberOfSlicesEdit;
    QPushButton *createArrayButton;
    QGridLayout *widthsGridLayout;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_13;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_3;
    QLabel *label_7;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *calculateButton;
    QPushButton *calculateVolumeButton;
    QCheckBox *addKnifeObjectCheck;
    QWidget *knifeObjectWidget;
    QGridLayout *gridLayout;
    QHBoxLayout *knifeObjectlLayout;
    QLabel *label_8;
    QSpacerItem *horizontalSpacer_3;
    QLineEdit *knifeObjectEdit;
    QPushButton *browseForKnifeObjectButton;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_14;
    QSpacerItem *horizontalSpacer_4;
    QLineEdit *outputDirectoryEdit;
    QPushButton *browseForOutputDirectoryButton;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *cutModelButton;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(559, 560);
        QIcon icon;
        icon.addFile(QStringLiteral(":/logo.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetMaximumSize);
        bannerPicture = new QLabel(centralWidget);
        bannerPicture->setObjectName(QStringLiteral("bannerPicture"));
        bannerPicture->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(bannerPicture);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(10);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        modelFilePathEdit = new QLineEdit(centralWidget);
        modelFilePathEdit->setObjectName(QStringLiteral("modelFilePathEdit"));

        horizontalLayout->addWidget(modelFilePathEdit);

        browseButton = new QPushButton(centralWidget);
        browseButton->setObjectName(QStringLiteral("browseButton"));

        horizontalLayout->addWidget(browseButton);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(9);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_4->addWidget(label_2);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_4->addWidget(label_3);

        modelSizeXEdit = new QLineEdit(centralWidget);
        modelSizeXEdit->setObjectName(QStringLiteral("modelSizeXEdit"));
        modelSizeXEdit->setMaximumSize(QSize(70, 16777215));
        modelSizeXEdit->setReadOnly(true);

        horizontalLayout_4->addWidget(modelSizeXEdit);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_4->addWidget(label_4);

        modelSizeYEdit = new QLineEdit(centralWidget);
        modelSizeYEdit->setObjectName(QStringLiteral("modelSizeYEdit"));
        modelSizeYEdit->setMaximumSize(QSize(70, 16777215));
        modelSizeYEdit->setReadOnly(true);

        horizontalLayout_4->addWidget(modelSizeYEdit);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout_4->addWidget(label_6);

        modelSizeZEdit = new QLineEdit(centralWidget);
        modelSizeZEdit->setObjectName(QStringLiteral("modelSizeZEdit"));
        modelSizeZEdit->setMaximumSize(QSize(70, 16777215));
        modelSizeZEdit->setReadOnly(true);

        horizontalLayout_4->addWidget(modelSizeZEdit);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(10);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        horizontalLayout_2->addWidget(label_9);

        cutAxisCombo = new QComboBox(centralWidget);
        cutAxisCombo->setObjectName(QStringLiteral("cutAxisCombo"));

        horizontalLayout_2->addWidget(cutAxisCombo);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));

        horizontalLayout_2->addWidget(label_10);

        numberOfSlicesEdit = new QLineEdit(centralWidget);
        numberOfSlicesEdit->setObjectName(QStringLiteral("numberOfSlicesEdit"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(20);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(numberOfSlicesEdit->sizePolicy().hasHeightForWidth());
        numberOfSlicesEdit->setSizePolicy(sizePolicy);
        numberOfSlicesEdit->setMinimumSize(QSize(20, 0));
        numberOfSlicesEdit->setMaximumSize(QSize(30, 16777215));
        numberOfSlicesEdit->setMaxLength(5);

        horizontalLayout_2->addWidget(numberOfSlicesEdit);

        createArrayButton = new QPushButton(centralWidget);
        createArrayButton->setObjectName(QStringLiteral("createArrayButton"));

        horizontalLayout_2->addWidget(createArrayButton);


        verticalLayout->addLayout(horizontalLayout_2);

        widthsGridLayout = new QGridLayout();
        widthsGridLayout->setSpacing(6);
        widthsGridLayout->setObjectName(QStringLiteral("widthsGridLayout"));
        widthsGridLayout->setSizeConstraint(QLayout::SetMinimumSize);
        widthsGridLayout->setHorizontalSpacing(6);
        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));

        widthsGridLayout->addWidget(label_11, 0, 0, 1, 1);

        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));

        widthsGridLayout->addWidget(label_12, 1, 0, 1, 1);

        label_13 = new QLabel(centralWidget);
        label_13->setObjectName(QStringLiteral("label_13"));

        widthsGridLayout->addWidget(label_13, 2, 0, 1, 1);


        verticalLayout->addLayout(widthsGridLayout);

        lineEdit_2 = new QLineEdit(centralWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));

        verticalLayout->addWidget(lineEdit_2);

        lineEdit_3 = new QLineEdit(centralWidget);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));

        verticalLayout->addWidget(lineEdit_3);

        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        verticalLayout->addWidget(label_7);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy1);

        verticalLayout->addWidget(label_5);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(40);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(40, -1, 40, 10);
        calculateButton = new QPushButton(centralWidget);
        calculateButton->setObjectName(QStringLiteral("calculateButton"));

        horizontalLayout_5->addWidget(calculateButton);

        calculateVolumeButton = new QPushButton(centralWidget);
        calculateVolumeButton->setObjectName(QStringLiteral("calculateVolumeButton"));

        horizontalLayout_5->addWidget(calculateVolumeButton);


        verticalLayout->addLayout(horizontalLayout_5);

        addKnifeObjectCheck = new QCheckBox(centralWidget);
        addKnifeObjectCheck->setObjectName(QStringLiteral("addKnifeObjectCheck"));
        addKnifeObjectCheck->setChecked(true);

        verticalLayout->addWidget(addKnifeObjectCheck);

        knifeObjectWidget = new QWidget(centralWidget);
        knifeObjectWidget->setObjectName(QStringLiteral("knifeObjectWidget"));
        knifeObjectWidget->setMinimumSize(QSize(0, 0));
        gridLayout = new QGridLayout(knifeObjectWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetMinimumSize);
        gridLayout->setVerticalSpacing(0);
        gridLayout->setContentsMargins(-1, 0, -1, 0);
        knifeObjectlLayout = new QHBoxLayout();
        knifeObjectlLayout->setSpacing(0);
        knifeObjectlLayout->setObjectName(QStringLiteral("knifeObjectlLayout"));
        label_8 = new QLabel(knifeObjectWidget);
        label_8->setObjectName(QStringLiteral("label_8"));

        knifeObjectlLayout->addWidget(label_8);

        horizontalSpacer_3 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        knifeObjectlLayout->addItem(horizontalSpacer_3);

        knifeObjectEdit = new QLineEdit(knifeObjectWidget);
        knifeObjectEdit->setObjectName(QStringLiteral("knifeObjectEdit"));

        knifeObjectlLayout->addWidget(knifeObjectEdit);

        browseForKnifeObjectButton = new QPushButton(knifeObjectWidget);
        browseForKnifeObjectButton->setObjectName(QStringLiteral("browseForKnifeObjectButton"));

        knifeObjectlLayout->addWidget(browseForKnifeObjectButton);


        gridLayout->addLayout(knifeObjectlLayout, 0, 0, 1, 1);


        verticalLayout->addWidget(knifeObjectWidget);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setSizeConstraint(QLayout::SetDefaultConstraint);
        label_14 = new QLabel(centralWidget);
        label_14->setObjectName(QStringLiteral("label_14"));

        horizontalLayout_6->addWidget(label_14);

        horizontalSpacer_4 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_4);

        outputDirectoryEdit = new QLineEdit(centralWidget);
        outputDirectoryEdit->setObjectName(QStringLiteral("outputDirectoryEdit"));

        horizontalLayout_6->addWidget(outputDirectoryEdit);

        browseForOutputDirectoryButton = new QPushButton(centralWidget);
        browseForOutputDirectoryButton->setObjectName(QStringLiteral("browseForOutputDirectoryButton"));

        horizontalLayout_6->addWidget(browseForOutputDirectoryButton);


        verticalLayout->addLayout(horizontalLayout_6);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(20);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(30, -1, 30, 30);
        cutModelButton = new QPushButton(centralWidget);
        cutModelButton->setObjectName(QStringLiteral("cutModelButton"));

        horizontalLayout_3->addWidget(cutModelButton);


        verticalLayout->addLayout(horizontalLayout_3);


        gridLayout_2->addLayout(verticalLayout, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        QWidget::setTabOrder(modelFilePathEdit, browseButton);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "3dSlicer", 0));
        label->setText(QApplication::translate("MainWindow", "Load model", 0));
        browseButton->setText(QApplication::translate("MainWindow", "Browse", 0));
        label_2->setText(QApplication::translate("MainWindow", "Model size (cm):", 0));
        label_3->setText(QApplication::translate("MainWindow", "X:", 0));
        modelSizeXEdit->setText(QString());
        label_4->setText(QApplication::translate("MainWindow", "Y:", 0));
        modelSizeYEdit->setText(QString());
        label_6->setText(QApplication::translate("MainWindow", "Z:", 0));
        modelSizeZEdit->setText(QString());
        label_9->setText(QApplication::translate("MainWindow", "Cut axis:", 0));
        cutAxisCombo->clear();
        cutAxisCombo->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "x", 0)
         << QApplication::translate("MainWindow", "y", 0)
        );
        label_10->setText(QApplication::translate("MainWindow", "Number of slices:", 0));
        numberOfSlicesEdit->setText(QString());
        createArrayButton->setText(QApplication::translate("MainWindow", "Create array", 0));
        label_11->setText(QApplication::translate("MainWindow", "Slice:", 0));
        label_12->setText(QApplication::translate("MainWindow", "Width:", 0));
        label_13->setText(QApplication::translate("MainWindow", "Fixed?", 0));
        label_7->setText(QString());
        label_5->setText(QString());
        calculateButton->setText(QApplication::translate("MainWindow", "Calculate Width", 0));
        calculateVolumeButton->setText(QApplication::translate("MainWindow", "Calculate Volume", 0));
        addKnifeObjectCheck->setText(QApplication::translate("MainWindow", "Add custom slicer knife object", 0));
        label_8->setText(QApplication::translate("MainWindow", "Knife object:", 0));
        browseForKnifeObjectButton->setText(QApplication::translate("MainWindow", "Browse", 0));
        label_14->setText(QApplication::translate("MainWindow", "Output directory:", 0));
        browseForOutputDirectoryButton->setText(QApplication::translate("MainWindow", "Browse", 0));
        cutModelButton->setText(QApplication::translate("MainWindow", "Cut model", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
