/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../_cpp_3d_slicer/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[19];
    char stringdata0[390];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 20), // "widthEdit_textEdited"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 4), // "text"
QT_MOC_LITERAL(4, 38, 11), // "showWarning"
QT_MOC_LITERAL(5, 50, 7), // "message"
QT_MOC_LITERAL(6, 58, 13), // "enableButtons"
QT_MOC_LITERAL(7, 72, 25), // "on_cutModelButton_clicked"
QT_MOC_LITERAL(8, 98, 23), // "on_browseButton_clicked"
QT_MOC_LITERAL(9, 122, 28), // "on_createArrayButton_clicked"
QT_MOC_LITERAL(10, 151, 26), // "on_calculateButton_clicked"
QT_MOC_LITERAL(11, 178, 35), // "on_cutAxisCombo_currentIndexC..."
QT_MOC_LITERAL(12, 214, 5), // "index"
QT_MOC_LITERAL(13, 220, 32), // "on_calculateVolumeButton_clicked"
QT_MOC_LITERAL(14, 253, 30), // "on_addKnifeObjectCheck_toggled"
QT_MOC_LITERAL(15, 284, 7), // "checked"
QT_MOC_LITERAL(16, 292, 37), // "on_browseForKnifeObjectButton..."
QT_MOC_LITERAL(17, 330, 17), // "onCutModelSuccess"
QT_MOC_LITERAL(18, 348, 41) // "on_browseForOutputDirectoryBu..."

    },
    "MainWindow\0widthEdit_textEdited\0\0text\0"
    "showWarning\0message\0enableButtons\0"
    "on_cutModelButton_clicked\0"
    "on_browseButton_clicked\0"
    "on_createArrayButton_clicked\0"
    "on_calculateButton_clicked\0"
    "on_cutAxisCombo_currentIndexChanged\0"
    "index\0on_calculateVolumeButton_clicked\0"
    "on_addKnifeObjectCheck_toggled\0checked\0"
    "on_browseForKnifeObjectButton_clicked\0"
    "onCutModelSuccess\0"
    "on_browseForOutputDirectoryButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   79,    2, 0x08 /* Private */,
       4,    1,   82,    2, 0x08 /* Private */,
       6,    0,   85,    2, 0x08 /* Private */,
       7,    0,   86,    2, 0x08 /* Private */,
       8,    0,   87,    2, 0x08 /* Private */,
       9,    0,   88,    2, 0x08 /* Private */,
      10,    0,   89,    2, 0x08 /* Private */,
      11,    1,   90,    2, 0x08 /* Private */,
      13,    0,   93,    2, 0x08 /* Private */,
      14,    1,   94,    2, 0x08 /* Private */,
      16,    0,   97,    2, 0x08 /* Private */,
      17,    0,   98,    2, 0x08 /* Private */,
      18,    0,   99,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   15,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->widthEdit_textEdited((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->showWarning((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->enableButtons(); break;
        case 3: _t->on_cutModelButton_clicked(); break;
        case 4: _t->on_browseButton_clicked(); break;
        case 5: _t->on_createArrayButton_clicked(); break;
        case 6: _t->on_calculateButton_clicked(); break;
        case 7: _t->on_cutAxisCombo_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->on_calculateVolumeButton_clicked(); break;
        case 9: _t->on_addKnifeObjectCheck_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->on_browseForKnifeObjectButton_clicked(); break;
        case 11: _t->onCutModelSuccess(); break;
        case 12: _t->on_browseForOutputDirectoryButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
