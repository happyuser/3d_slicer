/****************************************************************************
** Meta object code from reading C++ file 'calculatevolumeworker.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../_cpp_3d_slicer/calculatevolumeworker.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'calculatevolumeworker.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_CalculateVolumeWorker_t {
    QByteArrayData data[11];
    char stringdata0[108];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CalculateVolumeWorker_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CalculateVolumeWorker_t qt_meta_stringdata_CalculateVolumeWorker = {
    {
QT_MOC_LITERAL(0, 0, 21), // "CalculateVolumeWorker"
QT_MOC_LITERAL(1, 22, 13), // "statusChanged"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 4), // "text"
QT_MOC_LITERAL(4, 42, 15), // "progressChanged"
QT_MOC_LITERAL(5, 58, 5), // "value"
QT_MOC_LITERAL(6, 64, 16), // "progress2Changed"
QT_MOC_LITERAL(7, 81, 8), // "finished"
QT_MOC_LITERAL(8, 90, 5), // "error"
QT_MOC_LITERAL(9, 96, 3), // "err"
QT_MOC_LITERAL(10, 100, 7) // "process"

    },
    "CalculateVolumeWorker\0statusChanged\0"
    "\0text\0progressChanged\0value\0"
    "progress2Changed\0finished\0error\0err\0"
    "process"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CalculateVolumeWorker[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,
       4,    1,   47,    2, 0x06 /* Public */,
       6,    1,   50,    2, 0x06 /* Public */,
       7,    0,   53,    2, 0x06 /* Public */,
       8,    1,   54,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    0,   57,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void CalculateVolumeWorker::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CalculateVolumeWorker *_t = static_cast<CalculateVolumeWorker *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->statusChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->progressChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->progress2Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->finished(); break;
        case 4: _t->error((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->process(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (CalculateVolumeWorker::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CalculateVolumeWorker::statusChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (CalculateVolumeWorker::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CalculateVolumeWorker::progressChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (CalculateVolumeWorker::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CalculateVolumeWorker::progress2Changed)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (CalculateVolumeWorker::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CalculateVolumeWorker::finished)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (CalculateVolumeWorker::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CalculateVolumeWorker::error)) {
                *result = 4;
                return;
            }
        }
    }
}

const QMetaObject CalculateVolumeWorker::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_CalculateVolumeWorker.data,
      qt_meta_data_CalculateVolumeWorker,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CalculateVolumeWorker::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CalculateVolumeWorker::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CalculateVolumeWorker.stringdata0))
        return static_cast<void*>(const_cast< CalculateVolumeWorker*>(this));
    return QObject::qt_metacast(_clname);
}

int CalculateVolumeWorker::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void CalculateVolumeWorker::statusChanged(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CalculateVolumeWorker::progressChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void CalculateVolumeWorker::progress2Changed(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void CalculateVolumeWorker::finished()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void CalculateVolumeWorker::error(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
