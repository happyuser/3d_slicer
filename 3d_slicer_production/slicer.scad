//version of script is 1.3.14. HappyuserS company. Visit http://happyuser.info

include <TOUL.scad>;
//include <strings.scad>;
names = ["torus.stl","cone.stl","cube.stl","monkey.stl","r4_2.stl","torus_translated.stl","CENTERED.stl","UNIT3_fixed3.stl","frog_e1.stl","Ball_Bearing_7.stl"];
modules = ["module.stl"];

user_file=names[9];   // must be initalised
user_filename=user_file; // param1 passed via -D on cmd-line
echo("cutting user file:",user_file);

slice1=0;   // must be initalised
p_slice1=user_file; // param1 passed via -D on 

p_s_slice_size = "0,500,1000"; //"0,314,369,683,997"; //"0,200,1000";//"0,150,300,600,800"; //cm

p = "2"; //part number

axis = "x";

u_min = 0;
u_max = 20;

x0 = 0;
x1 = 1;//the margins of the slice that are calculated on a C++ side


// get init from comand line sample 
// Line sample 
//-D axis="x" -D n="4" -D p="1" -D p_s_slice_size="0,314,369,683,997" -D u_ma_x="43000" -D u_ma_y="63000" -D u_mi_x="-46650" -D u_mi_y="-44804"
// replace role is @-D ([^=]*)="([^"]*)"@im to  $1=$2;
 axis="x";  n=2;  p="4";  
 p_s_slice_size="0,314,369,683,997";  
 u_ma_x=43;  u_ma_y=63;  u_mi_x=-46.65;  u_mi_y=-44.804;
 u_s_x=27.8896; u_s_y=27.9447; u_s_z=8;
////////////////////////////////////

p_n  = p;
s_slice_size  = p_s_slice_size;
u_s = u_max - u_min;

echo("Axis:", axis);
echo("u_s:", u_s);
    echo("u_min:", u_min);
    echo("u_max:", u_max);
    

//u_s_x = 27889/1000;
//u_s_x = u_s_x /1000;

cube_size = 10*max(u_s_x,u_s_y,u_s_z);//TODO: make it 2*max(u_s_x,u_s_y,u_s_z )
echo("cube_size :",cube_size);
echo(str("user model resized to: ", u_s));

a_slice_size = split(s_slice_size , ",");
echo("Got slices points array:", a_slice_size);
echo("Geting slice #", p_n);
//slice s_slice_size  by ','
//function strToNbr(str, i=0, nb=0) = i == len(str) ? nb : nb+strToNbr(str, i+1, search(str[i],"0123456789")[0]*pow(10,len(str)-i-1));


module u_object()
{
    
    //resize([u_s,0,0], auto=true) //scales to 100 x size.
        //color([0.5,0,0,0.2]) 
    //translate([20,0,0])
    import(user_filename, convexity=3, center=true);
    echo("2. got user file! ", user_filename);
    
}

//u_object();

slices_c =  len(a_slice_size);
sum = 0;
for(x = [0:1:slices_c-1]) { 
	a = atoi(a_slice_size[x]);
    //echo(a);
	sum = sum + a;
    //echo("sum ", sum );
}


//echo("sum",sum);
//for(x = [0:1:0]) {//slices_c-1
//echo(a_slice_size[x]);
part_border_max = 0;
x = atoi(p_n)-1;//0;

sum_x = 0;
//a = 0;
for(i = [0:1:x]) { 
	a = atoi(a_slice_size[i]);
    //echo(a);
	sum_x = (sum_x + a);
}

//echo("sum_x ", sum_x );

    x_0 = u_min - cube_size + x0;
//u_min - cube_size + (atoi(a_slice_size[x])/1000)*u_s; 
    x_1 = u_min + x1;
//u_min + (atoi(a_slice_size[x+1])/1000)*u_s; 
echo(str("slice start:", x_0,"(cm.)"));
echo(str("slice end:", x_1,"(cm.)"));

difference(){
difference(){
color([0.5,0.5,0,0.2]) u_object();
    
color([0,0.5,0.5,0.2]) 
    if(axis == "y")
        translate([-cube_size/2,x_0,-cube_size/2])cube(size = cube_size, center = false);
    else
        translate([x_0,-cube_size/2,-cube_size/2])cube(size = cube_size, center = false);
}
color([0,0.5,0.5,0.2])
    if(axis == "y")
    translate([-cube_size/2,x_1,-cube_size/2])cube(size = cube_size, center = false);
    else 
        translate([x_1,-cube_size/2,-cube_size/2])cube(size = cube_size, center = false);
}
echo("Done.");
echo(str("se result in: ../scad_output/r",p_n,".stl"));
