                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1237203
The OpenScad Useful Library by roipoussiere is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

TOUL: The OpenScad Usefull Library
==================================

This file aims to provide a set of functions for vectors, strings and number operations.

- Author: Nathanaël Jourdane
- Email: nathanael@jourdane.net
- Date: december 30, 2015
- License: Creative Commons CC-BY (Attribution)
- thingiverse: http://www.thingiverse.com/thing:1237203

Requires OpenScad 2015.03 or sup.

I love implementing these recursive functions! :3
If you need a generic function that doesn't exists in OpenScad, or if you find
a bug in these ones, feel free to ask me. ;-)

An other string library: http://www.thingiverse.com/thing:526023

#### Instructions ####

Copy-paste the function(s) you need from TOUL.scad into your .scad file;

OR:

Place TOUL.scad in your local OpenSCAD folder under `/libraries`.
On Windows, this folder should default to `Documents/OpenSCAD/libraries`.
Then import TOUL.scad with the following line of code:

	include TOUL.scad

#### Summary ####

- [vect] **new_vector**(`len`, `[val]`): Returns a vector with `len` elements initialised to the `val` value.
- [any **type]**(`v`, `key`): Returns the value corresponding to the key `key` by searching in `v`, a vector of [key, value], or returns -1 if not found.
- [str] **strcat**(`v`, `[sep]`): Returns a string of a concatenated vector of substrings `v`, with an optionnaly
- [vect] **split**(`str`, `[sep]`): Returns a vector of substrings by cutting the string `str` each time where `sep` appears.
- [vect] **str2vec**(`str`): Returns a vector of chars, corresponding to the string `str`.
- [str] **substr**(`str`, `[pos]`, `[len]`): Returns a substring of a string.
- [str] **fillstr**(`substring`, `nb_occ`): Returns a string filled with a set of `substring`s, `nb_occ` times.
- [vect] **dec2bin**(num [,len]): Returns a vector of booleans corresponding to the binary value of the num `num`.
- [int] **atoi**(`str`, `[base]`): Returns the numerical form of a string `str`.

-------
Vectors
-------

----------------------
#### new_vector ####

[vect] new_vector(`len`, `[val]`)
Returns a vector with `len` elements initialised to the `val` value.

** Arguments **

- [int] `len`: The length of the vector.
- [any type] `val`: The values filled in the vector *(0 by default)*.

** Usage **

	echo(new_vector(5)); // [0, 0, 0, 0, 0]
	echo(new_vector(3, "a")); // ["a", "a", "a"]

------------------
#### getval ####

[any type] getval(`v`, `key`)
Returns the value corresponding to the key `key` by searching in `v`, a vector of [key, value], or returns -1 if not found.

** Arguments **

- [vect] `v`: A vector of [key, value] (aka a dictionnary).
- `key`: The key to search.

** Usage **

	dic = [["a", 12], ["b", 42], ["c", 29]];
	echo(getval(dic, "b")); // 42
	echo(getval(dic, "x")); // undef

-------
Strings
-------

------------------
#### strcat ####

[str] strcat(`v`, `[sep]`)
Returns a string of a concatenated vector of substrings `v`, with an optionnaly
separator `sep` between each.
See also: split()

** Arguments **

- [vect] `v`: The vector of string to concatenate.
- [str] `sep` (optional): A separator which will added between each substrings ("" by default).

** Usage **

	v = ["OpenScad", "is", "a", "free", "CAD", "software."];
	echo(strcat(v)); // "OpenScadisafreeCADsoftware."
	echo(strcat(v, " ")); // "OpenScad is a free CAD software."

-----------------
#### split ####

[vect] split(`str`, `[sep]`)
Returns a vector of substrings by cutting the string `str` each time where `sep` appears.
See also: strcat(), str2vec()

** Arguments **

- [str] `str`: The original string.
- [char] `sep`: The separator who cuts the string (" " by default).

** Usage **

	str = "OpenScad is a free CAD software.";
	echo(split(str)); // ["OpenScad", "is", "a", "free", "CAD", "software."]
	echo(split(str)[3]); // "free"
	echo(split("foo;bar;baz", ";")); // ["foo", "bar", "baz"]

-------------------
#### str2vec ####

[vect] str2vec(`str`)
Returns a vector of chars, corresponding to the string `str`.
See also: split()

** Arguments **

- [str] `str`: The original string.

** Usage **

	echo(str2vec("foo")); // ["f", "o", "o"] 

------------------
#### substr ####

[str] substr(`str`, `[pos]`, `[len]`)
Returns a substring of a string.

** Arguments **

- [str] `str`: The original string.
- [int] `pos` (optional): The substring position (0 by default).
- [int] `len` (optional): The substring length (string length by default).

** Usage **

	str = "OpenScad is a free CAD software.";
	echo(substr(str, 12)); // "a free CAD software."
	echo(substr(str, 12, 10)); // "a free CAD"
	echo(substr(str, len=8)); // or substr(str, 0, 8); // "OpenScad"

-------------------
#### fillstr ####

[str] fillstr(`substring`, `nb_occ`)
Returns a string filled with a set of `substring`s, `nb_occ` times.

** Arguments **

- [str] `str`: The substring to copy several times.
- [int] `nb_occ`: The number of occurence of the substring.

** Usage **

	echo(fillstr("6", 3)); // ECHO: "666"
	echo(fillstr("hey", 3)); // ECHO: "heyheyhey"

-------
Numbers
-------

-------------------
#### dec2bin ####

[vect] dec2bin(num [,len])
Returns a vector of booleans corresponding to the binary value of the num `num`.

** Arguments **

- [int] `num`: The number to convert.
- [int] `len` (optional): The vector length. If specified, fill the most significant bits with 0.

** Usage **

	echo(dec2bin(42)); // [1, 0, 1, 0, 1, 0]
	echo(dec2bin(42, 8)); // [0, 0, 1, 0, 1, 0, 1, 0]

----------------
#### atoi ####

[int] atoi(`str`, `[base]`)
Returns the numerical form of a string `str`.

** Arguments **

- [str] `str`: The string to converts (representing a number).
- [int] `base` (optional): The base conversion of the number
		(2 for binay, 10 for decimal (default), 16 for hexadecimal).

** Usage **

	echo(atoi("491585")); // 491585
	echo(atoi("-15")); // -15
	echo(atoi("01110", 2)); // 14
	echo(atoi("D5A4", 16)); // 54692
	echo(atoi("-5") + atoi("10") + 5); // 10