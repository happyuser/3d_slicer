3d_stl_model slicer version 1.3.21
HappyuseS team. visit http://happyuser.info
***
Instalation & Use
------------

Program definately works with OpenScad 2015.03. Not tested with other versions. 

1. Install OpenScad 2015.03 to your system. Take it from web.
2. Edit settings.ini file with path to OpenScad like on this screnshot http://prntscr.com/aqwd1l .
3. Edit path to admesh.exe like on this screenshot. Tested with version admesh-win32-0.98.2 .
4. check if slicer.scad script is in the same folder. 
5. check if TOUL.scad is in the same folder.
6. Run it and slice most files. 
7. Program will rewrite files in scad_output folder. So remember to save them. 
8. Use KISSlicer included in the bundle to try to fix your models. Or view what problems do they have
9. Model needs to be manifold 2 to be properly cutted. 
10. If no fatal errors - program will report "done" at the end of work. 
11. Errors and full report of work you can see log.txt which is updated on each operation.  
12. Array of slices is not limited. Be aware of it. 

screencast preasentation of demo version. Almost the same works final version. 
http://screencast.com/t/oiwC3sAXZtNc
http://screencast.com/t/fNjgOdnj4i

Full report of conversation on making this program can be found in full_conversation.txt
or original here 
https://docs.google.com/document/d/1ME57s2g4tIIXa6VCrdrSFD5eHNExbkoMuyyCV3VuAqA/edit?usp=sharing

***
Known bugs:
-------------

1. Can not cut far away origin models like this one 7070_4_c.stl
2. Fails on model with errors like this one 20-30-50-hollow.stl
3. fails on model with bugs CENTERED.stl - but model can be fixed
4. This fixed models with all, known by me, ways - fails on Y cat when cut deals with broken triangles frog_e1.stl
5. Groot support 200 center_fixed(0).stl - this one has problems and needs to be fixed manualy.
6. The same with this UNIT3_fixed3_ks_fix.stl 
7. For some models - no errors rise but model does not cut - just stays the same - like this one CENTERED.stl

***
Contacts
---------

HappyuseS team. visit http://happyuser.info
open scad developer: tarasprystavskyj@gmail.com
C++ qt5 developer: sergiyvovk69@gmail.com

