cube_size = 100;
u_s = 10;

knife_filename = "cube_wave.stl";

module knife_object()
{
    
    resize([40,0,0], auto=true) //scales to 100 x size.
        //color([0.5,0,0,0.2]) 
        translate([0,0,0])
            import(knife_filename, convexity=3, center=true);
            //import(knife_filename, convexity=3, center=true);
        echo("2. got knife file! ", knife_filename);
}

difference(){
  import("torus.stl", convexity=3, center=true);
    
  //cube(size = cube_size, center = false);
translate([-10,0,0])  
knife_object();

//translate([10,0,0])  
//knife_object();

}
