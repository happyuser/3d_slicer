//version of script is 1.3.14. HappyuserS company. Visit http://happyuser.info
include <TOUL.scad>;
//include <strings.scad>;
names = ["torus.stl","20-30-50-hollow_ks_fix.stl","cube.stl","monkey.stl","r4_2.stl","torus_translated.stl","stl_samples_to_cut/CENTERED_ks_fix2.stl","UNIT3_fixed3.stl","frog_e1.stl","Ball_Bearing_7.stl"];
modules = ["module.stl"];

user_file=names[0];   // must be initalised
user_filename=user_file; // param1 passed via -D on cmd-line
echo("Creating cut preview for user file:",user_file);

slice1=0;   // must be initalised
p_slice1=user_file; // param1 passed via -D on 

p_s_slice_size = "0,250,500,750,1000"; //"0,314,369,683,997"; //"0,200,1000";//"0,150,300,600,800"; //cm

p = "1"; //part number

////////////////////////////////////
axis="y"; 
n=4; 
p="1"; 
//"0,314,369,683,997"; 
p_s_slice_size = "0,250,500,750,1000"; 
u_s_x=57.8896; 
u_s_y=27.9447; 
u_s_z=8; 

u_max=14.0019; 
u_min=-13.8877; 

use_custom_slice_object=0; 
x0=0; 
x1=13.9448;


//torus
u_mi_x = -13.8877;
u_mi_y = -14;
u_mi_z = -4;

////////////////////////////////////

//axis = "x";


// get init from comand line sample 
// Line sample 
//-D axis="x" -D n="4" -D p="1" -D p_s_slice_size="0,314,369,683,997" -D u_ma_x="43000" -D u_ma_y="63000" -D u_mi_x="-46650" -D u_mi_y="-44804"
// replace role is @-D ([^=]*)="([^"]*)"@im to  $1=$2;


p_n  = p;
s_slice_size  = p_s_slice_size;
u_s = u_max - u_min;

echo("Axis:", axis);
echo("u_s:", u_s);
    echo("u_min:", u_min);
    echo("u_max:", u_max);

cube_size = max(u_s_x,u_s_y,u_s_z)*1.05;//TODO: make it 2*max(u_s_x,u_s_y,u_s_z )
echo("cube_size :",cube_size);
echo(str("user model resized to: ", u_s));

echo("u_s_x :",u_s_x);
echo("u_s_y :",u_s_y);
echo("u_s_z :",u_s_z);

echo("u_s_x :",u_s_x);
echo("u_s_y :",u_s_y);
echo("u_s_z :",u_s_z);

a_slice_size = split(s_slice_size , ",");
echo("Got slices points array:", a_slice_size);

module u_object()
{
    
    //resize([u_s,0,0], auto=true) //scales to 100 x size.
        //color([0.5,0,0,0.2]) 
    //translate([20,0,0])
    import(user_filename, convexity=3, center=true);
    echo("2. got user file! ", user_filename);
    
}

slices_c =  len(a_slice_size)-1;// розмір масиву
sum = 0;
for(x = [0:1:slices_c-1]) { 
	a = atoi(a_slice_size[x]);
    //echo(a);
	sum = sum + a;
    //echo("sum ", sum );
}

part_border_max = 0;
x = atoi(p_n)-1;//0;

    x_0 = u_min + x0; // позиція крайньої межі куба
    x_1 = u_min + x1;

use_custom_slice_object=1;
knife_filename = "cube_wave.stl";
//simple_cube = "cube_test.stl";


color([0.5,0.5,0,0.2]) u_object();



for(i = [1:1:slices_c-1]) { 
	a = atoi(a_slice_size[i]);   
    if(axis == "y") 
    {
      by = u_mi_y + (a-1000/2)/1000*u_s_y ;
    translate([u_mi_x,by,u_mi_z]) rotate( a = 90, v = [1, 0, 0] ) cube([u_s_x*1.1,u_s_y*1.1,0.1],true);
    }
    if(axis == "x")   
    {
      bx = u_mi_x+(a-1000/2)/1000*u_s_x ;
    translate([bx,u_mi_y,u_mi_z]) rotate( a = 90, v = [0, 1, 0] ) cube([u_s_x*1.1,u_s_y*1.1,0.1],true);
    }
}

echo("Done.");
echo(str("se result in: ../tmp/preview.stl"));
