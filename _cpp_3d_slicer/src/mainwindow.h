#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QCheckBox>
#include <QHBoxLayout>
#include "canvas.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

	Ui::MainWindow *ui;
	bool isOperationCanceled;

	// Елементи верхної таблиці:
	QList<QLabel *> slicesNumbersLabels;
	QList<QLineEdit *> widthsEdits;
	QList<QCheckBox *> fixedChecks;

	QMap<QString, float> modelSize;
	double fullVolume;

	QStringList savedFilesNames; // Імена файлів які збереглись після порізки 3D моделі.
	QMap<QString, QString> settings;

	// Calculate volume result:
	QMap<int, double> sliceVolume; // Масив, в який зберігаються обчислення об'ємів.
	QVector<double> slicesCutPoints;
	QVector<double> slicesWidths;

private slots:
	void widthEdit_onTextEdited(const QString& text);
	void cutPointEdit_onTextEdited(const QString& text);
	void showWarning(QString message);
	void enableButtons();
	void on_splitModelButton_clicked();
	void on_browseButton_clicked();
	void on_createArrayButton_clicked();
	void on_previewButton_clicked();
	void on_calculateButton_clicked();
	void on_cutAxisCombo_currentIndexChanged(int index);
    void on_calculateVolumeButton_clicked();
	void on_browseForKnifeObjectButton_clicked();
	void onMinusButtonClicked();
	void onPlusButtonClicked();
	void onCalculateVolumeReady();
	void onCutModelSuccess();

private:
	void setStyle();
	void loadSettings();
	void loadModelToPreview();
	void calculateSlicesWidths();
	bool convertBinaryStlFileToAscii(QString& outputFilePath);
	void setButtonsEnabledState(bool isEnabled);
	void createWidthsTable();
	QString getColor(const QString &element, const QString &defaultValue);
	bool getModelSizeAndType();
	void calculateWidth();
	void calculateVolume();
	void calculateSlicersWidthsFromCutPoints();
	void writeToLog(const QString &s);

	QMap<QString, QString> colors;
	Canvas *canvas;
	bool isStlModelBinary;

	// Елементи нижної таблиці:
	QHBoxLayout *table2_horizontalLayout;
	QList<QLabel *> table2_slicesNumbersLabels;
	QList<QPushButton *> table2_minusButtons;
	QList<QPushButton *> table2_plusButtons;
	QList<QLineEdit *> table2_widthsEdits;
	QList<QLineEdit *> table2_volumesEdits;
	QList<QLineEdit *> table2_cutPointsEdits;

	QMap<int, double> fixSlices;
};
#endif // MAINWINDOW_H
