#include "stlobject.h"

STLObject::STLObject() {}
STLObject::~STLObject() {}

/**
*stl uzantılı dosyayı oku
*dosyanın her satırını rows vectoründe tut
*@param okunacak dosya adı
*/
void STLObject::loadFile(const char *filename)
{
    ifstream file;
    file.open(filename);
    if(!file.is_open())
    {
        cout << "Dosya okunamadı. Dosya adı: " << filename << endl;
    }
    else
    {
        string line;
        while(!file.eof())
        {
            getline(file, line);
            rows.push_back(line);
        }
    }

    file.close();
}

/**
vertexleri -> vertex vector e ata
normalleri -> normal vector e ata
*/
void STLObject::parse()
{
    string line;
	for (unsigned int i = 0; i < rows.size(); i++)
    {
        line = rows[i];
        char *ch;
        ch = new char[line.size()+1];
        strcpy(ch,line.c_str());

        GLfloat xyz[3];

        // ======== VERTEX ========
        if( (ch[6]=='v' && ch[7]=='e' && ch[8]=='r' && ch[9]=='t' && ch[10]=='e' && ch[11]=='x') ||
            (ch[2]=='v' && ch[3]=='e' && ch[4]=='r' && ch[5]=='t' && ch[6]=='e' && ch[7]=='x') ||
            (ch[0]=='v' && ch[1]=='e' && ch[2]=='r' && ch[3]=='t' && ch[4]=='e' && ch[5]=='x') )
        {
            sscanf(ch,"\tvertex %f %f %f", &xyz[0], &xyz[1], &xyz[2]);
            Coordinate coord(&xyz[0]);
            vertex.push_back(coord);
        }

        // ======== FACE NORMAL ========
        if( (ch[2]=='f' && ch[3]=='a' && ch[4]=='c' && ch[5]=='e' && ch[6]=='t') ||
            (ch[0]=='f' && ch[1]=='a' && ch[2]=='c' && ch[3]=='e' && ch[4]=='t')
           )
        {
            sscanf(ch, "\tfacet normal %f %f %f", &xyz[0], &xyz[1], &xyz[2]);
            Coordinate coord(&xyz[0]);
            normal.push_back(coord);
        }
    }
}



vector<Coordinate> STLObject::getVertex()
{
    return vertex;
}

vector<Coordinate> STLObject::getNormals()
{
    return normal;
}


// http://content.gpwiki.org/index.php/OpenGL:Tutorials:Tutorial_Framework:Light_and_Fog
// Normal hesaplama
/**
*@param vec1, vec2, vec 3 : GLfloat
*Ucgenin 3 noktasinin koordinat degerleri ile "normal" hesaplanir
*@return hesaplanan "normal" in koordinat degeri dondurulur.
*/
Coordinate STLObject::computeFaceNormal(GLfloat* vec1, GLfloat* vec2 ,GLfloat* vec3)
{
	Coordinate result;

    GLfloat v1x, v1y, v1z, v2x, v2y, v2z;
    GLfloat nx, ny, nz;
    GLfloat vLen;

    // Calculate vectors
    v1x = vec1[X] - vec2[X];
    v1y = vec1[Y] - vec2[Y];
    v1z = vec1[Z] - vec2[Z];

    v2x = vec2[X] - vec3[X];
    v2y = vec2[Y] - vec3[Y];
    v2z = vec2[Z] - vec3[Z];

    // Get cross product of vectors
    nx = (v1y * v2z) - (v1z * v2y);
    ny = (v1z * v2x) - (v1x * v2z);
    nz = (v1x * v2y) - (v1y * v2x);

    // Normalise final vector

    vLen = sqrt( (nx * nx) + (ny * ny) + (nz * nz) );
    //cout << "vlen: " << vLen << endl;
    GLfloat val[3];
    if(vLen!=0)
    {
        val[X] = (nx / vLen);
        val[Y] = (ny / vLen);
        val[Z] = (nz / vLen);
    }
    else
    {
        val[X] = 0;
        val[Y] = 0;
        val[Z] = 0;
    }
    result.setCoordinate(val);

    return result;
}
