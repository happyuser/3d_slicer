#ifndef PROGRESSDIALOG_H
#define PROGRESSDIALOG_H
#include <QDialog>
#include <QLabel>
#include <QProgressBar>
#include <QPushButton>
#include <QTimer>
class ProgressDialog : public QDialog {
	Q_OBJECT
public:
	explicit ProgressDialog(QWidget *parent = 0, bool isSimple = true);
	~ProgressDialog();
public slots:
	void onTimeout();
	void onCancelButtonClicked();
	void setStatus(QString text);
	void setProgress(int value);
	void setProgress2(int value);
private:
	QLabel *elapsedTimeLabel;
	QLabel *status;
	QProgressBar *progressBar;
    QProgressBar *progressBar2;
	QPushButton *cancelButton;
	QTimer *timer;
	qint64 beginTime;
};
#endif // PROGRESSDIALOG_H
