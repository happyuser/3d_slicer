#include <GL/gl.h>
#ifndef COORDINATE_H
#define COORDINATE_H
class Coordinate {
public:
    Coordinate();
    virtual ~Coordinate();
    Coordinate(GLfloat *_xyz);
    void setCoordinate(GLfloat *_xyz);
    GLfloat* getCoordinate();
    GLfloat getCoordinate(int _index);
private:
    GLfloat xyz[3];
    enum {
        X,
        Y,
        Z
    };
};
#endif // COORDINATE_H
