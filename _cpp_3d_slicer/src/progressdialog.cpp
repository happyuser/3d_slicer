#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QProgressBar>
#include <QDateTime>
#include "progressdialog.h"
#include"mainwindow.h"

ProgressDialog::ProgressDialog(QWidget *parent, bool isSimple) :
		QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint) {
	setWindowTitle(isSimple ? "Progress" : "Volume Calculation Progress");
	QVBoxLayout *layout1 = new QVBoxLayout; QHBoxLayout *layout2 = new QHBoxLayout;
	elapsedTimeLabel = new QLabel;
	layout1->addWidget(elapsedTimeLabel);

	if (isSimple) {
		status = NULL;
		progressBar2 = NULL;
	} else {
		status = new QLabel;
		layout1->addWidget(status);
	}

	progressBar = new QProgressBar;
	layout1->addWidget(progressBar);

	if (!isSimple) {
		progressBar2 = new QProgressBar;
		layout1->addWidget(progressBar2);
		progressBar->setFormat("%p% - overall");
		progressBar2->setFormat("%p% - step");
		layout1->addWidget(new QLabel(
			"This process will generate new cutting points based on volume canculation."));
	}

	cancelButton = new QPushButton("Cancel");
	layout2->addStretch();
	layout2->addWidget(cancelButton);
	layout2->addStretch();
	layout1->addLayout(layout2);

	setLayout(layout1);
	beginTime = 0;
	timer = new QTimer();
	connect(timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
	connect(cancelButton, SIGNAL(clicked()), this, SLOT(onCancelButtonClicked()));
	timer->start(1000);
	setModal(true);
}

ProgressDialog::~ProgressDialog() {
	delete timer;
}

void ProgressDialog::onTimeout() {
	if (beginTime == 0) beginTime = QDateTime::currentMSecsSinceEpoch() - 1000;
	elapsedTimeLabel->setText("Elapsed time " +
		QDateTime::fromMSecsSinceEpoch(
		QDateTime::currentMSecsSinceEpoch() - beginTime).toUTC().toString("H:mm:ss"));

}

void ProgressDialog::onCancelButtonClicked() {
	((MainWindow*)parent())->isOperationCanceled = true;
	cancelButton->setEnabled(false);
}

void ProgressDialog::setStatus(QString text) { status->setText(text); }
void ProgressDialog::setProgress(int value) { progressBar->setValue(value); }
void ProgressDialog::setProgress2(int value) { progressBar2->setValue(value); }
