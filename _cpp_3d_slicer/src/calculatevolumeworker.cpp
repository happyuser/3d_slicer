#include <QProcess>
#include <QDir>
#include <QDebug>
#include <cmath>
#include "calculatevolumeworker.h"
#include "ui_mainwindow.h"
#include <QElapsedTimer>
#include <math.h>

CalculateVolumeWorker::CalculateVolumeWorker(MainWindow* mainWnd) :
	tmpVolume(), tmpPoints() {
	mainWindow = mainWnd;
}

//CalculateVolumeWorker::~CalculateVolumeWorker() {}

void CalculateVolumeWorker::process() {
	calculateVolume();
	//	mainWindow->setButtonsEnabledState(true); Можливо це причина глюка.
	emit finished();
}

void CalculateVolumeWorker::calculateVolume() {
	// У потрібному місці потрібно викликати emit progressChanged(value).
	// value має бути від 1 до 100.
	// С.Вовк.

	double precision = mainWindow->settings["precision"].toDouble();
	int i, W = 0, F = 0;

	// it will be our final array of cut points absolute coordinates starting from the start of model on the cut axis
	QVector<double> cutPoints;
	// temp iterators for cicles
	QVector<double>::iterator j;
	QVector<double>::iterator z;

	double x0, // x1,
			n0, n1;
	double fixWidthSum = 0;

	// Absolute size of the model in mm on cutting axis
	Size = ((mainWindow->ui->cutAxisCombo->currentText() == "x") ?
				mainWindow->ui->modelSizeXEdit->text().toDouble() :
				mainWindow->ui->modelSizeYEdit->text().toDouble());

	// Підрахунок загальної довжити фіксованих шматків fixWidthSum
	for (i = 0; i < mainWindow->fixedChecks.count(); i++)
		if (mainWindow->fixedChecks[i]->checkState() == Qt::Checked) {
			F++;
			fixWidthSum += mainWindow->widthsEdits[i]->text().toDouble();
		}

	// If the final summ of fixed slices sizes is more then the size of model - rise error.
	if (fixWidthSum >= Size) {
		emit error("Error: width fixed slices"); // перевірити граматику
		return;
	}

	// If the sum is equal to the model size - rise error.
	if (F == mainWindow->widthsEdits.count()) {
		emit error("All slices is fixed"); // перевірити граматику
		return;
	}


	W = mainWindow->widthsEdits.count() - F;  //W - is the number of not fixed parts

	double slicePrecision = precision/W; //we get slice absolute precission


	// calculating volume for slice
	double equalVolume = 0, equalSize = 0;

	// we have a full wolume of the model - and though get the volume of the non fixed cuts
	equalVolume = mainWindow->fullVolume / W;
	qDebug() << "W" << W;

	//зробити перевірку, чи модель взагалі подільна
	//вивести на форму середній об’єм?

	//  що відносно похибок
	x0 = 0;
	//x1 = Size;
	// Вовк: x1 присвоюєтся але ніде не використовується. Перевірете.

	n0 = 0; // we put start of current slice on the left mergin
	n1 = Size; // and the end of slice on the end of model
	double ser = 0;

	cutPoints.append(x0);
	//cutPoints[0] = ser;

	// percentage of phase 1
	int progress0 = 0; // for a overal process
	int progress1 = 0; // for a "for" cicle
	int progress2 = 0; // for a internal while cicle
	int progress2t = 0; // for a internal while cicle - temp value to test if it is grater then prev value of  progress2


	// lets find out the time elapsed on the next big while cicle
	QElapsedTimer myTimer;
	myTimer.start();
	QString status_message;
	//	status_message = "phase 1 - 'Placing non fixed slices'";
	status_message = "1 of 3. Calculating non-fixed slices.";
	qDebug() << "<<<<<< Start of" << status_message;
	emit statusChanged(status_message);

	// перший етап порізки - The first module of cut.
	// Here we find the points for non fixed slices for them to be with equal volumes
	// Non fixed slices are said to be much biger then a fixed one.
	// for this we use "method of or center cut"
	for (i = 0; i < W - 1; i ++) { //var i here is a current order number of a not fixed slice. For every one of them we need to find
		//calculate progress1
		progress1 = static_cast<int>((i / (W - 1)) * 100);
		progress0 = static_cast<int>(0 + progress1 / 3);
		qDebug() << "progress1_1 is" << progress1 << "%";
		qDebug() << "progress0 is" << progress0 << "%";
		emit progressChanged(progress0);
		progress2 = 0;
		emit progress2Changed(progress2);

		float temp_start_prec = fabs(equalVolume - tmpVolume[i]) - precision; // це початковий шлях який необхідно пройти алгоритму
		// при наближенні до деобхідної точності
		//their left and right margins so that all of them will have almost equal sizes
		// We serch the points of cut for the current non fixed slice
		while (fabs(equalVolume - tmpVolume[i]) > precision) { // do while the tmpVolume[i] is not close anough to equalVolume
			if (mainWindow->isOperationCanceled) return;

			//calculate progress2
			progress2t = static_cast<int>( ( 1 - (fabs(fabs(equalVolume - tmpVolume[i]) - precision))/temp_start_prec )  * 100/2);
			if(progress2t > progress2)
				progress2 = progress2t;
			qDebug() << "progress1_2 is" << progress2 << "%";
			progress0 = static_cast<int>( 0+ 33/(W-1)*i + (progress2 / 3/(W-1)) );
			emit progressChanged(progress0);
			emit progress2Changed(progress2);

			//TODO: має ще бути обмеження по максимальній точності по ширині
			ser = (n0 + n1) / 2; // On each iteration we cut
			qDebug() << "ser" << ser;

			if (!cut(x0, ser, i)) return; // we try the cut on that center point on the left from it
			if (!getVolume("tmp", i)) return; // and get volume of the first slice

			qDebug() << "delta" << fabs(equalVolume - tmpVolume[i]);// its our absolute mistake of a current cut - its cumming to 0mm
			qDebug() << "n0" << n0;
			qDebug() << "n1" << n1;

			// if the volume needed is more then the volume of slice to the left of ser point
			if (equalVolume > tmpVolume[i])
				n0 = ser; // then - we go to search the cut point on the left slice
			else
				n1 = ser; //else - we got to the rigth slice (second one)
		} // and go to the next - deeper iteration and try to find our point which will cut the volume needed.
		//init starting cut points for next slice
		x0 = n0 = ser; // left point will be the point that we just found - the right point of prev slice
		n1 = Size; // the right point is the end of 3d model
		cutPoints.append(ser); // add our just found cut point to the array
		mainWindow->sliceVolume[i] = tmpVolume[i]; // add just found slice volume to the array.

	}

	int nMilliseconds = myTimer.elapsed();
	qDebug() << "time elapsed on phase 1 =" << nMilliseconds/1000 << "s.";
	qDebug() << ">>>>>>>>> End of phase 1";


	// the last step of the algorithm
	if (!cut(x0, Size, W - 1)) return;
	if (!getVolume("tmp", W - 1)) return;
	mainWindow->sliceVolume[i] = tmpVolume[i];
	cutPoints.append(Size);
	// the last step of the algorithm


	//mainWindow->ui->label_7->setText("Delta Relative:  " + QString::number(i));

	// закінчення першого етапу порізки

	qDebug() << "cutPoints" << cutPoints;
	qDebug() << "equalVolume" << equalVolume;


	// наступний крок. Встановлюємо фіксовані шматочки.


	//    cutPoints.clear();

	//    equalSize = Size / W;


	//    for (i = 0; i < W; i++)
	//        cutPoints.append(i*equalSize); // просто розміщуємо по рівних частинах

	//    cutPoints.append(Size);


	qDebug() << "cutPoints" << cutPoints;


	tmpPoints = cutPoints;

	//QVectorIterator<double> k(tmpPoints);

	double tmpW = 0;

	int s;


	double max = 0, maxFixWidthSum = 0;

	for (i = 0; i < mainWindow->fixedChecks.count(); i++) {
		if (mainWindow->fixedChecks[i]->checkState() == Qt::Checked) {
			while (i < mainWindow->widthsEdits.count() &&
				   mainWindow->fixedChecks[i]->checkState() == Qt::Checked) {
				maxFixWidthSum += mainWindow->widthsEdits[i]->text().toDouble();
				i++;
			}
			if (max < maxFixWidthSum) max = maxFixWidthSum;
			maxFixWidthSum = 0;
		}
	}

	qDebug() << "max" << max;


	double minVolumeWidth = Size;

	for (i = 0; i < W; i++)
		if (tmpPoints[i + 1] - tmpPoints[i] < minVolumeWidth)
			minVolumeWidth = tmpPoints[i + 1] - tmpPoints[i];

	qDebug() << "minVolumeWidth" << minVolumeWidth;

	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// PHASE 2
	// lets find out the time elapsed on the next big while cicle
	myTimer.start();
	nMilliseconds = myTimer.elapsed();
	//	status_message = "phase 2 - 'Placing fixed slices'";
	status_message = "2 of 3. Calculating fixed slices.";
	qDebug() << "<<<<<< Start of" << status_message;
	emit statusChanged(status_message);


	// Here we put a fixed width slices somewhere on the cut points of non fixed slices
	if (max < minVolumeWidth) {
		for (i = 0, s = 0, j = tmpPoints.begin(); i < mainWindow->widthsEdits.count();
				s = 0, tmpW = 0, i++, j++) {
			if (mainWindow->isOperationCanceled) return;

			//qDebug() << "( i / (mainWindow->widthsEdits.count())" << ( i / (mainWindow->widthsEdits.count()) );
			//calculate progress1
			progress1 = static_cast<int>(( i / (mainWindow->widthsEdits.count()) ) * 100);
			progress0 = static_cast<int>(33 + progress1 / 3);
			qDebug() << "progress2_1 is " << progress1 << "%";
			emit progressChanged(progress0);
			progress2 = 0;
			emit progress2Changed(progress2);


			if (i == 0 && mainWindow->fixedChecks[i]->checkState() == Qt::Checked) {
				while (mainWindow->fixedChecks[i]->checkState() == Qt::Checked) {
					j = tmpPoints.insert(j + 1,(*j + mainWindow->widthsEdits[i]->text().toDouble()));
					qDebug() << tmpPoints;
					i++;
				}
			} else
				if (mainWindow->fixedChecks[i]->checkState() == Qt::Checked) {
					while ((i + s) < mainWindow->widthsEdits.count() &&
						   mainWindow->fixedChecks[i + s]->checkState() == Qt::Checked) {
						tmpW += mainWindow->widthsEdits[i + s]->text().toDouble();
						qDebug() << "s" << s;
						qDebug() << "fix" << mainWindow->widthsEdits[i + s]->text().toDouble();
						s++;
					}

					qDebug() << "tmpW" << tmpW;

					if (i + s < mainWindow->widthsEdits.count()) {
						*j = *j - tmpW / 2;
						while (mainWindow->fixedChecks[i]->checkState() == Qt::Checked) {
							j = tmpPoints.insert(j + 1,(*j + mainWindow->widthsEdits[i]->text().toDouble()));
							qDebug() << tmpPoints;
							i++;
						}
					} else {
						*j = *j - tmpW;
						while (i < mainWindow->widthsEdits.count() - 1 &&
							   mainWindow->fixedChecks[i]->checkState() == Qt::Checked) {
							tmpPoints.append(*j + mainWindow->widthsEdits[i]->text().toDouble());
							qDebug() << tmpPoints;
							j = tmpPoints.end();
							i++;
						}
						tmpPoints.append(Size);
					}
				}
		}
	} else {
		equalSize = (Size - fixWidthSum) / W;

		tmpPoints.clear();
		tmpPoints.append(0);

		for (i = 0, j = tmpPoints.begin(); i < mainWindow->widthsEdits.count() - 1;
			 i++, j++) {
			if (mainWindow->fixedChecks[i]->checkState() == Qt::Checked)
				tmpPoints.append(*j + mainWindow->widthsEdits[i]->text().toDouble());
			else tmpPoints.append(*j + equalSize);
			qDebug() << "tmpPoints" << tmpPoints;
		}
		tmpPoints.append(Size);
	}

	nMilliseconds = myTimer.elapsed();
	qDebug() << "time elapsed on phase 2 =" << nMilliseconds / 1000 << "s.";
	qDebug() << ">>>>>>>>> End of phase 2";


	cutPoints = tmpPoints;
	qDebug() << "cutPoints" << cutPoints;


	for (i = 0, j = tmpPoints.begin(); i < mainWindow->widthsEdits.count(); i++, j++) {
		if (mainWindow->isOperationCanceled) return;
		if (!cut(*j, *(j + 1), i)) return;
		if (mainWindow->isOperationCanceled) return;
		if (!getVolume("tmp", i)) return;
	}


	double fixWidth = 0, delta = 0;
	double tmpPrecision = precision, tmp = 0, tmpV = 0; // виправити. поставити справді виміряне відхилення


	for (i = mainWindow->widthsEdits.count() - 1; i >= 0; i--)
		if (mainWindow->fixedChecks[i]->checkState() != Qt::Checked) { // що якщо тільки один елемент буде з обємом, всі інші - фіксовані?
			tmpV = tmpVolume[i];
			break;
		}

	for (i = 0; i < mainWindow->widthsEdits.count(); i++)
		if (mainWindow->fixedChecks[i]->checkState() != Qt::Checked) {
			tmp += fabs(tmpV - tmpVolume[i]);
			tmpV = tmpVolume[i];
			// тут тип в присвоюється
			// обчислення відхилення від середнього
		}

	if (tmp > precision) tmpPrecision = tmp;

	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// PHASE 3
	// lets find out the time elapsed on the next big while cicle
	myTimer.start();
	nMilliseconds = myTimer.elapsed();
	qDebug() << "<<<<<< Start of phase 3 - 'Mooving fixed slices till we have enough precision of volume cut'";
	// status_message = "phase 3 - 'Mooving fixed slices till we have enough precision of volume cut'";
	status_message = "3 of 3. Optimizing slices volume.";
	qDebug() << "<<<<<< Start of" << status_message;
	emit statusChanged(status_message);
	progress0 = 66;
	emit progressChanged(progress0);


	// progress2 may be we need it for user to fill that something is keeping going on ;)
	progress2 = 0;
	emit progress2Changed(progress2);

	//!!! додати що інакше - модель вже відповідає параметрам
	//! here we try to move fixed slices to the left or to the righ to find the point where they would cut the neibouring non fixed
	//! slices to the equal volumes
	while (tmpPrecision > precision) {
		// calculate progress1 with logarithmic scale
		progress1 = static_cast<int>(( 1 - (tmpPrecision - precision)/tmpPrecision  ) * 100);//
		progress0 = static_cast<int>(66 + progress1 / 3);
		qDebug() << "progress3_1 is" << progress1 << "%";
		emit progressChanged(progress0);

		for (i = 0, j = tmpPoints.begin(), z = tmpPoints.begin();
			 i < mainWindow->widthsEdits.count() - 1; i ++, j++) {

			if (mainWindow->fixedChecks[i]->checkState() == Qt::Checked) {
				qDebug() << i << "-------------continue------------";
				continue;
			} else {
				for (s = i + 1; s < mainWindow->widthsEdits.count(); s++)

					if (mainWindow->fixedChecks[s]->checkState() != Qt::Checked) {
						z = tmpPoints.begin() + s + 1;
						qDebug() << "z" << *z;
						break;

						// потрібно зробити глобальний вихід з циклу, якщо вже немає нефіксованого елемента.
					}

				if (s == mainWindow->widthsEdits.count()) break;

				qDebug() << "s" << s;
				qDebug() << "*(j + 1)" << *(j + 1);
				qDebug() << "*(z - 1)" << *(z - 1);


				if (mainWindow->fixedChecks[i + 1] -> checkState() != Qt::Checked) {
					n0 = *j;
					n1 = *z;
					int k = 0; // step of cicle
					float max_delta = 0;
					progress2 = 0;

					while ((delta = fabs(tmpVolume[s] - tmpVolume[i])) > slicePrecision
						   && (n1 - n0) > 0.000001) { // має ще бути обмеження по максимальній точності по ширині
						// qDebug() << "delta =" << delta;
						k++; // iterate cicle counts
						if(k == 2) max_delta = delta;

						//calculate progress2
						progress2t = static_cast<int>(( 1 - delta / max_delta ) * 100);
						if(progress2t > progress2) progress2 = progress2t;
						qDebug() << "progress3_2_1 is" << progress2 << "%";
						emit progress2Changed(progress2);

						if (tmpVolume[s] > tmpVolume[i]) n0 = *(j + 1);
						else n1 = *(j + 1);

						*(j + 1) = (n1 + n0) / 2; // перевірити

						// qDebug() << "ser" << *(j + 1);

						if (mainWindow->isOperationCanceled) return;
						if (!cut(*j,*(j + 1),i)) return;
						if (mainWindow->isOperationCanceled) return;
						if (!cut(*(j + 1),*z,s)) return;
						if (mainWindow->isOperationCanceled) return;
						if (!getVolume("tmp", i)) return;
						if (mainWindow->isOperationCanceled) return;
						if (!getVolume("tmp", s)) return;
					}
				} else {
					n0 = *j;
					n1 = *z;

					fixWidth = (*(z - 1) - *(j + 1));
					progress2 = 0;

					if (tmpVolume[s] > tmpVolume[i])
						while ((delta = fabs(tmpVolume[s] - tmpVolume[i])) > slicePrecision
							   && n1 - n0 > 0.000001) { // має ще бути обмеження по максимальній точності по ширині
							// qDebug() << "delta = " << delta;

							if (tmpVolume[s] > tmpVolume[i]) n0 = *(z - 1);
							else n1 = *(z - 1);

							*(z - 1) = (n0 + n1) / 2;

							*(j + 1) = *(z - 1) - fixWidth;

							// qDebug() << "ser" << *(j + 2);

							if (mainWindow->isOperationCanceled) return;
							if (!cut(*j,*(j + 1),i)) return;
							if (mainWindow->isOperationCanceled) return;
							if (!cut(*(z - 1),*z,s)) return;
							if (mainWindow->isOperationCanceled) return;
							if (!getVolume("tmp", i)) return;
							if (mainWindow->isOperationCanceled) return;
							if (!getVolume("tmp", s)) return;

							// calculate progress2
							progress2t = static_cast<int>(( slicePrecision / delta ) * 100);
							if(progress2t > progress2)
								progress2 = progress2t;
							qDebug() << "progress3_2_2 is" << progress2 << "%";
							emit progress2Changed(progress2);
						}
					else {

						while ((delta = fabs(tmpVolume[s] - tmpVolume[i])) > slicePrecision
							   && n1 - n0 > 0.000001) { // має ще бути обмеження по максимальній точності по ширині
							// qDebug() << "delta = " << delta;

							if (tmpVolume[s] > tmpVolume[i]) n0 = *(j + 1);
							else n1 = *(j + 1);

							*(j + 1) = (n0 + n1)/2;

							*(z - 1) = *(j + 1) + fixWidth;

							if (mainWindow->isOperationCanceled) return;
							if (!cut(*j,*(j + 1),i)) return;
							if (mainWindow->isOperationCanceled) return;
							if (!cut(*(z - 1),*z,s)) return;
							if (mainWindow->isOperationCanceled) return;
							if (!getVolume("tmp", i)) return;
							if (mainWindow->isOperationCanceled) return;
							if (!getVolume("tmp", s)) return;

							// calculate progress2
							progress2t = static_cast<int>((slicePrecision / delta ) * 100);
							if(progress2t > progress2) 	progress2 = progress2t;
							qDebug() << "progress3_2_3 is" << progress2 << "%";
							emit progress2Changed(progress2);

						}
					}
					for (int k = i + 1; k < s - 1; k++) tmpPoints[k + 1] =
							tmpPoints[k] + mainWindow->widthsEdits[k]->text().toDouble();
				}
			}
		}


		// calculating presicion
		for (i = mainWindow->widthsEdits.count() - 1; i >= 0; i--)
			if (mainWindow->fixedChecks[i]->checkState() != Qt::Checked) { // що якщо тільки один елемент буде з обємом, всі інші - фіксовані?
				tmpV = tmpVolume[i];
				break;
			}

		tmp = 0;

		for (i = 0; i < mainWindow->widthsEdits.count(); i++)
			if (mainWindow->fixedChecks[i]->checkState() != Qt::Checked) {
				tmp += fabs(tmpV - tmpVolume[i]);
				tmpV = tmpVolume[i];
				// тут тип в присвоюється обчислення відхилення від середнього
			}


		qDebug() << "tmp(precision)" << tmp;
		// qDebug() << "sliceVolume" << sliceVolume;
		qDebug() << "tmpVolume" << tmpVolume;
		// qDebug() << "cutPoints" << cutPoints;
		qDebug() << "tmpPoints" << tmpPoints;

		// перевірка, чи алгоритм збіжний і подальша його ініціалізація
		if (tmpPrecision > tmp) {
			tmpPrecision = tmp;
			cutPoints = tmpPoints;
			mainWindow->sliceVolume = tmpVolume;
			// алгоритм збіжний.
			// Будемо виконувати наступну ітерацію, поки не досягнута максимальна точність
		} else {
			emit error("Мodel is not appropriate for the type manifol 2 or other error.");
			break;
		}


		// отут поставив перевірку, що якщо алгоритм збіжний - замінити масиви,
		// якщо ні - зупинити цикл і вивести повідомлення, що модель не дорівнює маніфол 2,
		// або є інша помилка.

	}
	//------------------------------------------------------------------------------------
	// end general calculating

	nMilliseconds = myTimer.elapsed();
	qDebug() << "time elapsed on phase 3 =" << nMilliseconds / 1000 << "s.";
	qDebug() << ">>>>>>>>> End of phase 3";


	// last calculating wolume
	for (i = 0, j = cutPoints.begin(); i < mainWindow->widthsEdits.count(); i++, j++) {
		if (mainWindow->isOperationCanceled) return;
		if (!cut(*j,*(j+1), i)) return;
		if (mainWindow->isOperationCanceled) return;
		if (!getVolume("tmp", i)) return;
	}


	mainWindow->sliceVolume = tmpVolume;

	/*
	QString showVolume, showPoint("0   ");
	// show slice width
	for (i = 0, j = cutPoints.begin(); i < mainWindow->widthsEdits.count(); i++, j++) {
		showVolume += (QString::number(mainWindow->sliceVolume[i]) + "   ");
		showPoint += (QString::number(*(j + 1)) + "   ");
		mainWindow->widthsEdits[i]->setText(QString::number(*(j + 1) - *j));
	}
	// Show points cutting and slice volume.
	emit ready(showPoint.trimmed(), showVolume.trimmed());
	*/

	mainWindow->slicesCutPoints.clear();
	mainWindow->slicesWidths.clear();
	for (i = 0, j = cutPoints.begin(); i < mainWindow->widthsEdits.count(); i++, j++) {
		mainWindow->slicesCutPoints.append(*(j + 1));
		mainWindow->slicesWidths.append(*(j + 1) - *j);
	}
	emit ready();
} // Кінець функції calculateVolume.

bool CalculateVolumeWorker::cut(double x0, double x1, int i) {
	QString scadOutput = "tmp";
	qDebug() << "x0 =" << x0 << "x1 =" << x1;
	QStringList parameters, a; QProcess process; int r;
	QString standardOutput, standardError;
	QMap<QString, QString> definitions;
	// definitions will be passed to openscad after -D parametr.

	if (!QDir::setCurrent(QApplication::applicationDirPath())) return false;

	// Make the scadOutput directory if it does not exist:
	QDir dir(QApplication::applicationDirPath());
	if (!dir.exists(scadOutput)) {
		if (!dir.mkdir(scadOutput)) {
			emit error("Could not make " +
					   QDir::toNativeSeparators(QApplication::applicationDirPath() +
												scadOutput) + " directory.");
			return false;
		}
	}

	definitions["user_file"] =
			"\"" + mainWindow->ui->modelFilePathEdit->text().replace("\\", "\\\\") + "\"";


	definitions["u_s_x"] = QString::number(((mainWindow->modelSize["maxX"] - mainWindow->modelSize["minX"])));
	definitions["u_s_y"] = QString::number(((mainWindow->modelSize["maxY"] - mainWindow->modelSize["minY"])));
	definitions["u_s_z"] = QString::number(((mainWindow->modelSize["maxZ"] - mainWindow->modelSize["minZ"])));
	definitions["axis"] = "\"" + mainWindow->ui->cutAxisCombo->currentText() + "\"";


	if (mainWindow->ui->cutAxisCombo->currentText() == "x") {
		definitions["u_min"] = QString::number((mainWindow->modelSize["minX"]));
		definitions["u_max"] = QString::number((mainWindow->modelSize["maxX"]));
	} else {
		definitions["u_min"] = QString::number((mainWindow->modelSize["minY"]));
		definitions["u_max"] = QString::number((mainWindow->modelSize["maxY"]));
	}

	definitions["n"] = QString::number(mainWindow->widthsEdits.count());

	float xi = 0; int sum_left = 0, w;


	//!!! початок рудиментарного коду (який не буде потрібен у наступній версії)

	a.append("0");

	QVector<double>::iterator j;


	int n;
	for (n = 0, j = tmpPoints.begin(); n < mainWindow->widthsEdits.count(); n++, j++) {
		xi = *(j+1) - *j;
		w = (int)(xi / Size * 1000); // обчислюємо відносну величину
		//шматка - відносно розміру моделі по відповідній осі
		sum_left += +w; // обчислюємо точки порізки по тій відносній величині
		a.append(QString::number(sum_left));
	}


	definitions["p_s_slice_size"] = "\"" + a.join(",") + "\"";
	// Обчислення цього масиву - рудиментарне і пізніше ми його заберемо.


	parameters.clear();
	parameters.append("-o");


	definitions["x0"] = QString::number(x0);
	definitions["x1"] = QString::number(x1);

	parameters.append(QString(scadOutput) + QDir::separator() + "r" +
					  QString::number(i + 1) + ".stl");
	definitions["p"] = "\"" + QString::number(i + 1) + "\"";
	// The p definition is needed as string.

	{
		QMapIterator<QString, QString> iterator(definitions);
		while (iterator.hasNext()) {
			iterator.next();
			parameters.append("-D " + iterator.key() + "=" + iterator.value());
		}
	}

	parameters.append("slicer.scad");
	writeToLog("openscad parameters:\n" + parameters.join(" ") + "\n");

	// Start openscad and wait finish:
	process.start(mainWindow->settings["openscadExecutableFilePath"], parameters);
	process.waitForFinished(-1); // will wait forever until finished
	r = ((process.exitStatus() == QProcess::NormalExit) ? process.exitCode() : -1);
	standardOutput = process.readAllStandardOutput();
	standardError = process.readAllStandardError();
	writeToLog(
			"openscad result:\n"  + (standardOutput + standardError).trimmed() + "\n");

	if (r) {
		/*
		emit error("openscad parameters:\n" + parameters.join("\n") +
				   "\n-----------------------------------------------------------\nopenscad exit code: " +
				   QString::number(r) + "\nopenscad result:\n" + standardOutput + standardError);
		*/
		emit error("Error: 3D model is not manifold. Please check log.");
		return false;
	}

	return true;
} // Кінець функції cut.

bool CalculateVolumeWorker::getVolume(QString scad_output, int i) {
	QStringList parameters; QProcess process; int r;
	QString standardOutput, standardError;
	if (!mainWindow->settings.contains("admeshExecutableFilePath")) {
		emit error("There is no admeshExecutableFilePath in the settings.");
		return false;
	}

	parameters.append(QApplication::applicationDirPath() + "/" + scad_output + "/r" + QString::number(i + 1) + ".stl");
	process.start(mainWindow->settings["admeshExecutableFilePath"], parameters);
	process.waitForFinished(-1);
	r = ((process.exitStatus() == QProcess::NormalExit) ? process.exitCode() : -1);
	standardOutput = process.readAllStandardOutput(),
			standardError = process.readAllStandardError();
	if (r) {
		emit error("admesh exit code: " + QString::number(r) +
				   "\nadmesh result:\n" + standardOutput + standardError);
		return false;
	}

	QRegExp rx(".*Volume   :  ([-.\\d ]*)");

	if (rx.indexIn(standardOutput.remove('\r')) == -1) {
		emit error("Could not parse the admesh output.\n" + standardOutput);
		return false;
	}
	// we get the volume of the current cut
	tmpVolume[i] = rx.cap(1).trimmed().toDouble();
	// qDebug() << "tmpVolume" << i << "=" << tmpVolume[i];
	return true;
} // Кінець функції getVolume.

void CalculateVolumeWorker::writeToLog(const QString &s) {
	QString filePath(QApplication::applicationDirPath() + "/log.txt");
	QFile file(filePath);
	if (!file.open(QFile::Append)) {
		emit error("Error of open the \"" +
				   QDir::toNativeSeparators(filePath) + "\" file for write.");
		return;
	}
	file.write(s.toUtf8());
	file.close();
}
