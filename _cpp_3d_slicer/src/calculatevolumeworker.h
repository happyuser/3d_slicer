#ifndef CALCULATEVOLUMEWORKER_H
#define CALCULATEVOLUMEWORKER_H
#include <QObject>
#include "mainwindow.h"
class CalculateVolumeWorker : public QObject {
	Q_OBJECT
public:
	CalculateVolumeWorker(MainWindow* mainWnd);
signals:
	void statusChanged(QString text);
	void progressChanged(int value);
	void progress2Changed(int value);
//	void ready(QString cutPoints, QString slicesVolume);
	void ready();
	void finished();
	void error(QString err);
public slots:
	void process();
private:
	void calculateVolume();
	bool cut(double, double, int);
	bool getVolume(QString scad_output, int i);
	void writeToLog(const QString &s);
	MainWindow* mainWindow;
	double Size;
	QMap<int, double> tmpVolume;
	QVector<double> tmpPoints;
};
#endif // CALCULATEVOLUMEWORKER_H
