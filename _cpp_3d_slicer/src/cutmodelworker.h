#ifndef CUTMODELWORKER_H
#define CUTMODELWORKER_H
#include <QObject>
#include "mainwindow.h"
class CutModelWorker : public QObject {
	Q_OBJECT
public:
	explicit CutModelWorker(MainWindow* mainWnd);
signals:
	void progressChanged(int value);
	void finished();
	void success();
	void error(QString err);
public slots:
	void process();
private:
	void cutModel();
	void writeToLog(const QString &s);
	MainWindow* mainWindow;
};
#endif // CUTMODELWORKER_H
