#include <QApplication>
#include <QDir>
#include <QMouseEvent>
#include <GL/glu.h>
#include "canvas.h"

Canvas::Canvas(QWidget *parent, const QMap<QString, QString> &colors) :
		QGLWidget(parent),
		backgroundColor(colors["previewModelBackground"]), modelColor(colors["model"]),
		knifeXColor(colors["knifeX"]), knifeYColor(colors["knifeY"]) {
//	setFormat(QGLFormat(QGL::DoubleBuffer | QGL::DepthBuffer));
}

void Canvas::initializeGL() {
	static const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
	static const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
	static const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	static const GLfloat light_position[] = { 2.0f, 5.0f, 5.0f, 0.0f };
	static const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
	static const GLfloat mat_diffuse[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
	static const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
	static const GLfloat high_shininess[] = { 100.0f };

	glClearColor(
			backgroundColor.redF(), backgroundColor.greenF(), backgroundColor.blueF(),
			1.0f);
//	glShadeModel(GL_FLAT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);

	glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);
}

/*
void Canvas::paintEvent(QPaintEvent *event) {
    Q_UNUSED(event);
    QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.drawText(10, height() - 10, status);
}
*/

void Canvas::resizeGL(int width, int height) {
	const float x = (float) width / (float) height;
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-x, x, -1.0, 1.0, 2.0, 100.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
//	glClearDepth(1.0f);
	gluLookAt(0.0f, 0.0f, 10.0f,
			  0.0f, 0.0f,  0.0f,
			  0.0f, 1.0f,  0.0f);

/*
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	GLfloat x = GLfloat(width) / height;
	glFrustum(-x, x, -1.0, 1.0, 4.0, 15.0);
	glMatrixMode(GL_MODELVIEW);
*/
}

void Canvas::draw() {
	glTranslatef(moveX, moveY, moveZ);
	glScalef(scale, scale, scale);
	glRotatef(rotX, 1, 0, 0);
	glRotatef(rotY, 0, 1, 0);
	glRotatef(rotZ, 0, 0, 1);

//-----------------------------------------------------------------------------
	// Малювання ліній координати x, y, z.

	int lineLength = 500;

	glDisable(GL_LIGHTING);
	glLineWidth(1.5);
	glBegin(GL_LINES);

	glColor3ub(255, 0, 0);
	glVertex3i(-lineLength, 0, 0);
	glVertex3i(lineLength, 0, 0);

	glColor3ub(0, 255, 0);
	glVertex3i(0, -lineLength, 0);
	glVertex3i(0, lineLength, 0);

	glColor3ub(0, 0, 255);
	glVertex3i(0, 0, -lineLength);
	glVertex3i(0, 0, lineLength);

	glEnd();

//-------------------------------------------------------------------------------
	// Малювання моделі stl.

	glEnable(GL_CULL_FACE);
	glEnable(GL_LIGHTING);
	glDisable(GL_BLEND);
	glColor3ub(modelColor.red(), modelColor.green(), modelColor.blue());
	glBegin(GL_TRIANGLES);
	for (unsigned int i = 0; i < vertex.size(); i += 3) {
		GLfloat *fnormal = normals[(i) / 3].getCoordinate();
		GLfloat *fvertex = vertex[i].getCoordinate();
		GLfloat *fvertex2 = vertex[i + 1].getCoordinate();
		GLfloat *fvertex3 = vertex[i + 2].getCoordinate();
		//glNormal3fv(fnormal);

		 //Bazı modellerde "normal" degeri bulunmuyor. Orn; Blender'dan .stl ciktisi alindiginda
		 //"normal" degeri bulunmuyorsa, 3 noktanin koordinat bilgileri ile hesaplanir.
		 // ve ya binary stl den ascii ye cevirince "normal" degeri yok
		if ( fnormal[0] == 0 && fnormal[1] == 0 && fnormal[2] == 0 ) {
			Coordinate coord;
			coord = stl.computeFaceNormal(&fvertex[0], &fvertex2[0], &fvertex3[0]);
			glNormal3f(coord.getCoordinate(0), coord.getCoordinate(1),
					   coord.getCoordinate(2));
		} else glNormal3f(fnormal[0], fnormal[1], fnormal[2]);
		glVertex3fv(fvertex);
		glVertex3fv(fvertex2);
		glVertex3fv(fvertex3);
	}
	glEnd();

//--------------------------------------------------------------
	// Малювання ножів.
	if (!slicesWidths.empty()) {
		float position = cutAxis ? minModelY : minModelX;

		glDisable(GL_CULL_FACE);
		glDisable(GL_LIGHTING);
		glEnable(GL_BLEND); // Enable blending.
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // Set blending function.

		// Вибір кольору ножів в залежності від осі порізки.
		if (cutAxis == 0) // x
			glColor4ub(knifeXColor.red(), knifeXColor.green(), knifeXColor.blue(), 150);
		else // y
			glColor4ub(knifeYColor.red(), knifeYColor.green(), knifeYColor.blue(), 150);

		float knifeDiameter = knifeSize / 2.0;
		float modelSizeX = maxModelX - minModelX;
		float modelSizeY = maxModelY - minModelY;
		float modelSizeZ = maxModelZ - minModelZ;
		float modelCenterX = minModelX + (modelSizeX / 2.0);
		float modelCenterY = minModelY + (modelSizeY / 2.0);
		float modelCenterZ = minModelZ + (modelSizeZ / 2.0);

		float center_c1 = (cutAxis == 0) ? modelCenterY : modelCenterX;
		float c[] = { center_c1 - knifeDiameter, modelCenterZ - knifeDiameter,
					  center_c1 + knifeDiameter, modelCenterZ - knifeDiameter,
					  center_c1 + knifeDiameter, modelCenterZ + knifeDiameter,
					  center_c1 - knifeDiameter, modelCenterZ + knifeDiameter};

		glBegin(GL_QUADS);
		for (int i = 0; i < (slicesWidths.count() - 1); i++) {
			position += slicesWidths[i];
			for (int a = 0; a < 8; a += 2) {
				if (cutAxis == 0) // x
					glVertex3f(position, c[a], c[a + 1]);
				else // y
					glVertex3f(c[a], position, c[a + 1]);
			}
		}
		glEnd();
	}
}

void Canvas::paintGL() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPushMatrix();
	draw();
	glPopMatrix();
}
/*
int Canvas::getObjectIndex(const QPoint &pos) {
	const int MaxSize = 512;
	GLuint buffer[MaxSize];
	GLint viewport[4];

	glGetIntegerv(GL_VIEWPORT, viewport);
	glSelectBuffer(MaxSize, buffer);
	glRenderMode(GL_SELECT);

	glInitNames();
	glPushName(0);

//	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
//	glLoadIdentity();
	gluPickMatrix(GLdouble(pos.x()), GLdouble(viewport[3] - pos.y()),
			5.0, 5.0, viewport);
	GLfloat x = GLfloat(width()) / height();
//	glFrustum(-x, x, -1.0, 1.0, 4.0, 15.0);
	glFrustum(-x, x, -1.0, 1.0, 2.0, 100.0);

//	glMatrixMode(GL_MODELVIEW);
	gluLookAt(0.0f, 0.0f, 10.0f,
			  0.0f, 0.0f,  0.0f,
			  0.0f, 1.0f,  0.0f);
	draw();
//	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	if (!glRenderMode(GL_RENDER)) return -1;
	return buffer[3];
}
*/
void Canvas::mousePressEvent(QMouseEvent* event) {
	if (event->button() == Qt::LeftButton || event->button() == Qt::RightButton) {
		mousePos = event->pos();
		mousePressPos = mousePos;
		pressMoveX = moveX;
		pressMoveY = moveY;
		pressMoveZ = moveZ;
		setCursor(Qt::ClosedHandCursor);
//		if (event->button() == Qt::LeftButton)
//			std::cout << getObjectIndex(mouse_pos) << std::endl;
   }
}

void Canvas::mouseReleaseEvent(QMouseEvent* event) {
	if (event->button() == Qt::LeftButton || event->button() == Qt::RightButton)
		unsetCursor();
}

void Canvas::mouseMoveEvent(QMouseEvent* event) {
    auto p = event->pos();

	if (event->buttons() & Qt::LeftButton) {
		const float moveUnit = 0.01;
		auto d = p - mousePressPos;
		if (event->modifiers() & Qt::ControlModifier)
			moveZ = pressMoveZ + (d.y() * moveUnit);
		else {
			moveX = pressMoveX + (d.x() * moveUnit);
			moveY = pressMoveY - (d.y() * moveUnit);
		}
		updateGL();

	} else if (event->buttons() & Qt::RightButton) {
		auto d = p - mousePos;
		const float rotateUnit = 3.0;
		if (event->modifiers() & Qt::ControlModifier) {
			if (d.x() > 0) rotY += rotateUnit;
			else if (d.x() < 0) rotY -= rotateUnit;
		} else {
			if (d.x() > 0) rotZ += rotateUnit;
			else if (d.x() < 0) rotZ -= rotateUnit;
			if (d.y() > 0) rotX += rotateUnit;
			else if (d.y() < 0) rotX -= rotateUnit;
		}
		if (rotY < 0.0) rotY = 360.0;
		if (rotX < 0.0) rotX = 360.0;
		if (rotZ < 0.0) rotZ = 360.0;
		updateGL();
    }

	mousePos = p;
}

void Canvas::wheelEvent(QWheelEvent *event) {
	const float scaleUnit = 1.1;
	if (event->delta() < 0) scale *= scaleUnit;
	else if (event->delta() > 0) scale /= scaleUnit;
	updateGL();
}
