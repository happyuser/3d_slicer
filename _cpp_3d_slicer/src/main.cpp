#include "mainwindow.h"
#include <QApplication>
#include <QDesktopWidget>
#include <QMessageBox>
#define VERSION "1.7.3"

int main(int argc, char *argv[]) {
	QApplication a(argc, argv);
#ifdef QT_DEBUG
	QApplication::setApplicationName("Advanced 3D Model Splitter " VERSION " (debug version)");
#else
	QApplication::setApplicationName("Advanced 3D Model Splitter " VERSION);
#endif

// Under Windows make the interface font slightly biger.
#ifdef Q_OS_WIN
	QFont f(qApp->font());
	f.setPointSize(f.pointSize() + 3);
	qApp->setFont(f);
#endif

	if (!QGLFormat::hasOpenGL()) {
		QMessageBox::warning(NULL, qAppName(), "This system has no OpenGL support.");
		return 1;
	}

	MainWindow w;
	w.adjustSize();

#ifdef Q_OS_WIN
	QRect screenGeometry = QApplication::desktop()->screenGeometry();
	int x = (screenGeometry.width() - w.width()) / 2;
	int y = (screenGeometry.height() - w.height() - 50) / 2;
	w.move(x, y);
#endif

	w.show();
	return a.exec();
}
