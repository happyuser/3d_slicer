#include "coordinate.h"

Coordinate::Coordinate()
{
    xyz[X] = 0.0;
    xyz[Y] = 0.0;
    xyz[Z] = 0.0;
}

Coordinate::~Coordinate()
{
}

Coordinate::Coordinate(GLfloat *_xyz)
{
    xyz[X] = _xyz[X];
    xyz[Y] = _xyz[Y];
    xyz[Z] = _xyz[Z];
}

void Coordinate::setCoordinate(GLfloat *_xyz)
{
    xyz[X] = _xyz[X];
    xyz[Y] = _xyz[Y];
    xyz[Z] = _xyz[Z];
}

GLfloat* Coordinate::getCoordinate()
{
    return xyz;
}

GLfloat Coordinate::getCoordinate(int _index)
{
    if(_index >=0 && _index<3)
        return xyz[_index];

    return -1;
}
