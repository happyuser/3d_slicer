#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <vector>
#include <math.h>
#include "coordinate.h"
using namespace std;
#ifndef OBJECT_H
#define OBJECT_H
class STLObject {
public:
	STLObject();
	virtual ~STLObject();
	void loadFile(const char *filename);
	void parse();
	vector<Coordinate> getVertex();
	vector<Coordinate> getNormals();
	Coordinate computeFaceNormal(GLfloat* vec1, GLfloat* vec2, GLfloat* vec3);
	vector<string> rows;
	vector<Coordinate> vertex;
	vector<Coordinate> normal;
private:
	enum { X, Y, Z};
};
#endif // OBJECT_H
