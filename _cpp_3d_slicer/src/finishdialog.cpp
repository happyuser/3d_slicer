#include <QApplication>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QUrl>
#include <QDesktopServices>
#include <QMessageBox>
#include <QFileDialog>
#include <QDir>
#include "finishdialog.h"

FinishDialog::FinishDialog(QWidget *parent, const QStringList& modelsFilesNames) :
		QDialog(parent), savedFilesNames(modelsFilesNames) {
	QVBoxLayout *layout1 = new QVBoxLayout;
	QHBoxLayout *layout2 = new QHBoxLayout, *layout3 = new QHBoxLayout,
				*layout4 = new QHBoxLayout;
	QPushButton *browseButton = new QPushButton("Browse"),
				*openDirectoryButton = new QPushButton("Open directory"),
				*closeButton = new QPushButton("Close");
	saveButton = new QPushButton("Save");

	layout1->addWidget(new QLabel(
			"Process completed successfully.\nPlease select output folder and click save."));

	saveDirectoryEdit = new QLineEdit(
			QDir::toNativeSeparators(QApplication::applicationDirPath() + "/scad_output"));
	layout2->addWidget(saveDirectoryEdit);
	layout2->addWidget(browseButton);
	layout1->addLayout(layout2);

	layout3->addWidget(saveButton);
	layout3->addStretch();
	layout1->addLayout(layout3);

	status = new QLabel();
	layout1->addWidget(status);

	layout4->addWidget(openDirectoryButton);
	layout4->addWidget(closeButton);
	layout4->addStretch();
	layout1->addLayout(layout4);

	setLayout(layout1);
	setFixedWidth(600);

	connect(browseButton, SIGNAL(clicked()), this, SLOT(onBrowseButtonClicked()));
	connect(openDirectoryButton, SIGNAL(clicked()),
			this, SLOT(onOpenDirectoryButtonClicked()));
	connect(saveButton, SIGNAL(clicked()), this, SLOT(onSaveButtonClicked()));
	connect(closeButton, SIGNAL(clicked()), this, SLOT(accept()));
}

void FinishDialog::onBrowseButtonClicked() {
	QString s(QFileDialog::getExistingDirectory(this, "Select output directory"));
	if (!s.isEmpty()) saveDirectoryEdit->setText(QDir::toNativeSeparators(s));
}

void FinishDialog::onOpenDirectoryButtonClicked() {
	QDesktopServices::openUrl(QUrl::fromLocalFile(saveDirectoryEdit->text() + "/"));
}

void FinishDialog::onSaveButtonClicked() {
	QString saveDirectoryPath(saveDirectoryEdit->text());
	if (!QDir(saveDirectoryPath).exists()) {
		QMessageBox::warning(this, QApplication::applicationName(),
							 "The directory " + saveDirectoryPath + " deos not exist.");
		return;
	}

	// Remove old result files if exist.
	if (!QDir::setCurrent(saveDirectoryPath)) {
		QMessageBox::warning(this, QApplication::applicationName(),
				 "Could not set the " + saveDirectoryPath + " directory as current.");
		return;
	}
	int i;
	for (i = 0; i < savedFilesNames.count(); i++) {
		if (QFile::exists(savedFilesNames[i])) {
			if (!QFile::remove(savedFilesNames[i])) {
				QMessageBox::warning(this, QApplication::applicationName(),
						"Could not remove the " +
						saveDirectoryPath + QDir::separator() + savedFilesNames[i] + " file.");
				return;
			}
		}
	}

	QString tmpDirectoryPath(QApplication::applicationDirPath() + "/tmp");
	if (!QDir::setCurrent(tmpDirectoryPath)) {
		QMessageBox::warning(this, QApplication::applicationName(),
				"Could not set the " + QDir::toNativeSeparators(tmpDirectoryPath) +
				" directory as current.");
		return;
	}

	saveButton->setEnabled(false);
	status->setText("The result is saving.");
	qApp->processEvents();

	for (i = 0; i < savedFilesNames.count(); i++) {
		if (!QFile::copy(savedFilesNames[i],
						 saveDirectoryPath + QDir::separator() + savedFilesNames[i])) {
			QMessageBox::warning(this, QApplication::applicationName(),
					"Could not copy the " +
					QDir::toNativeSeparators(tmpDirectoryPath + "/" + savedFilesNames[i]) +
					" file to the " + saveDirectoryPath + " directory.");
			saveButton->setEnabled(true);
			status->setText("");
			return;
		}
		qApp->processEvents();
	}

	saveButton->setEnabled(true);
	status->setText("The result was saved.");
}
