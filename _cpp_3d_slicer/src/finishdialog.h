#ifndef FINISHDIALOG_H
#define FINISHDIALOG_H
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QStringList>
class FinishDialog : public QDialog {
	Q_OBJECT
public:
	FinishDialog(QWidget *parent, const QStringList& modelsFilesNames);
private slots:
	void onBrowseButtonClicked();
	void onOpenDirectoryButtonClicked();
	void onSaveButtonClicked();
private:
	QStringList savedFilesNames;
	QLabel *status;
	QLineEdit *saveDirectoryEdit;
	QPushButton *saveButton;
};
#endif // FINISHDIALOG_H
