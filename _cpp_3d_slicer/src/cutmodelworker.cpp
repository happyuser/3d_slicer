#include <QDir>
#include <QFile>
#include <QProcess>
#include <QDebug>
#include "cutmodelworker.h"
#include "ui_mainwindow.h"

CutModelWorker::CutModelWorker(MainWindow* mainWnd) {
	mainWindow = mainWnd;
}

void CutModelWorker::process() {
	cutModel();
//	mainWindow->setButtonsEnabledState(true);
	emit finished();
}

void CutModelWorker::cutModel() {
	QStringList parameters, a; QProcess process; int i, r;
	QString standardOutput, standardError;
	QMap<QString, QString> definitions;
	// definitions will be passed to openscad after -D parametr.

	//QString outputDirectoryPath(mainWindow->ui->outputDirectoryEdit->text());
	QString outputDirectoryPath(QApplication::applicationDirPath() + "/tmp");
	/*
	if (outputDirectoryPath.isEmpty()) {
		emit error("The output directory path is not specified.");
		return;
	}
	if (outputDirectoryPath.endsWith(QDir::separator())) {
		emit error("The separator must not be in the end of output directory path.");
		return;
	}
	*/

	// Make the output directory if it does not exist.
	/*
	if (!QDir(outputDirectoryPath).exists()) {
		i = outputDirectoryPath.lastIndexOf(QDir::separator());
		if (i == -1) {
			emit error("Could not find the separator in output directory path.");
			return;
		}
		{
			QDir dir(outputDirectoryPath.left(i));
			// dir is the parent direcory of outputDirectoryPath.
			if (!dir.exists()) {
				emit error("The " + dir.path() + " directory does not exist.");
				return;
			}
			if (!dir.mkdir(outputDirectoryPath.mid(i + 1))) {
				emit error("Could not make the " + outputDirectoryPath + " directory.");
				return;
			}
		}
	}
	*/
	if (!QDir(outputDirectoryPath).exists()) {
		if (!QDir(QApplication::applicationDirPath()).mkdir("tmp")) {
			emit error("Could not make the " + QDir::toNativeSeparators(outputDirectoryPath) +
					   " directory.");
			return;
		}
	}

	// Set outputDirectoryPath as current directory.
	if (!QDir::setCurrent(outputDirectoryPath)) {
		emit error("Could not set the " + QDir::toNativeSeparators(outputDirectoryPath) +
				   " directory as current.");
		return;
	}

	QString originalFileName;
	{
		QString s(QFileInfo(mainWindow->ui->modelFilePathEdit->text()).fileName());
		i = s.lastIndexOf('.');
		if (i == -1) {
			emit error("Could not find the \".\" in model file name.");
			return;
		}
		originalFileName = s.left(i);
	}

	definitions["user_file"] =
			"\"" + mainWindow->ui->modelFilePathEdit->text().replace("\\", "\\\\") + "\"";

	//definitions["u_s_x"] = QString::number((int)(ui->modelSizeXEdit->text().toFloat() * 10));
	//definitions["u_s_y"] = QString::number((int)(ui->modelSizeYEdit->text().toFloat() * 10));
	//definitions["u_s_z"] = QString::number((int)(ui->modelSizeZEdit->text().toFloat() * 10));

	definitions["u_s_x"] =
			QString::number(((mainWindow->modelSize["maxX"] - mainWindow->modelSize["minX"])));
	definitions["u_s_y"] =
			QString::number(((mainWindow->modelSize["maxY"] - mainWindow->modelSize["minY"])));
	definitions["u_s_z"] =
			QString::number(((mainWindow->modelSize["maxZ"] - mainWindow->modelSize["minZ"])));
	definitions["axis"] = "\"" + mainWindow->ui->cutAxisCombo->currentText() + "\"";

	definitions["use_custom_slice_object"] =
			mainWindow->ui->addKnifeObjectCheck->isChecked() ? "1" : "0";
	if (mainWindow->ui->addKnifeObjectCheck->isChecked())
		definitions["knife_filename"] =
				"\"" + mainWindow->ui->knifeObjectEdit->text().replace("\\", "\\\\") + "\"";

//	qDebug() << "axis: " << ui->cutAxisCombo->currentText();
	if (mainWindow->ui->cutAxisCombo->currentText() == "x") {
//		qDebug() << "axis: X";
		definitions["u_min"] = QString::number(mainWindow->modelSize["minX"]);
		definitions["u_max"] = QString::number(mainWindow->modelSize["maxX"]);
	} else { // definitions["axis"] == "\"y\""
//		qDebug() << "axis: Y";
		definitions["u_min"] = QString::number(mainWindow->modelSize["minY"]);
//		qDebug() << "minY: "<< definitions["u_min"];
		definitions["u_max"] = QString::number(mainWindow->modelSize["maxY"]);
	}

	definitions["n"] = QString::number(mainWindow->widthsEdits.count());
	definitions["u_mi_x"] = QString::number(mainWindow->modelSize["minX"]);
	definitions["u_mi_y"] = QString::number(mainWindow->modelSize["minY"]);
	definitions["u_mi_z"] = QString::number(mainWindow->modelSize["minZ"]);
	//definitions["u_ma_x"] = QString::number((int)(mainWindow->modelSize["maxX"])*1000);
	//definitions["u_ma_y"] = QString::number((int)(mainWindow->modelSize["maxY"])*1000);
	//definitions["u_ma_z"] = QString::number((int)(mainWindow->modelSize["maxZ"])*1000);

	float xi = 0, size_sum = 0; int sum_left = 0, w;
	//!!! початок рудиментарного коду (який не буде потрібен у наступній версії)
	a.append("0");
	for (i = 0; i < mainWindow->widthsEdits.count(); i++)
		size_sum += mainWindow->widthsEdits[i]->text().toFloat();
	for (i = 0; i < mainWindow->widthsEdits.count(); i++) {
		xi = mainWindow->widthsEdits[i]->text().toFloat();
		w = (int)(xi / size_sum * 1000); // обчислюємо відносну величину
		//шматка - відносно розміру моделі по відповідній осі
		sum_left += + w; // обчислюємо точки порізки по тій відносній величині
		a.append(QString::number(sum_left));
	}
	definitions["p_s_slice_size"] = "\"" + a.join(",") + "\"";
	//Обчислення цього масиву - рудиментарне і пізніше ми його заберемо.

	QString logFilePath(QApplication::applicationDirPath() + "/log.txt");
	if (QFile::exists(logFilePath)) {
		if (!QFile::remove(logFilePath)) {
			emit error("Could not remove the " + QDir::toNativeSeparators(logFilePath) +
					   " file.");
			return;
		}
	}

	QString outputFileName;
	double x0, x1;
	size_sum = 0;
	for (i = 1; i <= mainWindow->widthsEdits.count(); i++) {
		if (mainWindow->isOperationCanceled) return;
		if (i > 1)
			writeToLog("----------------------------------------------------------------\n");
		parameters.clear();

		outputFileName = originalFileName + "_" + QString::number(i) + ".stl";
		if (QFile::exists(outputFileName)) {
			if (!QFile::remove(outputFileName)) {
				emit error("Could not remove the " +
						QDir::toNativeSeparators(outputDirectoryPath + "/" + outputFileName +
						" file."));
				return;
			}
		}

		parameters.append("-o");
		parameters.append(outputFileName);

		// порахуємо ліву та праву межу в сантиметрах від лівого краю моделі.
		x0 = size_sum;
		size_sum += mainWindow->widthsEdits[i - 1]->text().toDouble();
		x1 = size_sum;
		// відповідно Х0 та Х1 будуть відрізнятись на довжину поточного відрізку.
		// і додамо їх тепер у параметри
		definitions["x0"] = QString::number(x0);
		definitions["x1"] = QString::number(x1);
		definitions["p"] = "\"" + QString::number(i) + "\"";
		// The p definition is needed as string.

		{
			QMapIterator<QString, QString> iterator(definitions);
			while (iterator.hasNext()) {
				iterator.next();
				parameters.append("-D " + iterator.key() + "=" + iterator.value());
			}
		}

		parameters.append(QApplication::applicationDirPath() + "/slicer.scad");
		writeToLog("openscad parameters:\n" + parameters.join(" ") + "\n");

		// Start openscad and wait finish.
		process.start(mainWindow->settings["openscadExecutableFilePath"], parameters);
		process.waitForFinished(-1); // will wait forever until finished
		r = ((process.exitStatus() == QProcess::NormalExit) ? process.exitCode() : -1);
		standardOutput = process.readAllStandardOutput(),
		standardError = process.readAllStandardError();

		writeToLog("\nVolume slice: " +
				   QString::number(mainWindow->sliceVolume[i - 1]) + "\n");
//!!! потрібно буде поправити запис обєму в лог файл так,
// щоб коли обчислення відбув. не по обєму, то він не виводивя.
		writeToLog(
			"openscad result:\n"  + (standardOutput + standardError).trimmed() + "\n");
        emit progressChanged(100 / mainWindow->widthsEdits.count() * i);

		if (r) {
			/*
			emit error(
"openscad parameters:\n" + parameters.join("\n") +
"\n-----------------------------------------------------------\nopenscad exit code: " +
QString::number(r) + "\nopenscad result:\n" + standardOutput + standardError);
			*/
			emit error("Error: 3D model is not manifold. Please check log.");
			return;
		}
		mainWindow->savedFilesNames.append(outputFileName);
	} // for (i = 1; i <= mainWindow->widthsEdits.count(); i++)

	emit success();
} // Кінець функції cutModel.

void CutModelWorker::writeToLog(const QString &s) {
	QString filePath(QApplication::applicationDirPath() + "/log.txt");
	QFile file(filePath);
	if (!file.open(QFile::Append)) {
		emit error("Error of open the \"" +
				   QDir::toNativeSeparators(filePath) + "\" file for write.");
		return;
	}
	file.write(s.toUtf8());
	file.close();
}
