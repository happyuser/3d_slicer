#ifndef CANVAS_H
#define CANVAS_H
#include <QtOpenGL/QGLWidget>
#include "coordinate.h"
#include "stlobject.h"
class Canvas : public QGLWidget {
    Q_OBJECT
public:
	Canvas(QWidget* parent, const QMap<QString, QString> &colors);
    void initializeGL();
	void resizeGL(int width, int height);
//	void paintEvent(QPaintEvent* event);
	void paintGL();

	STLObject stl;
	std::vector<Coordinate> vertex;
	std::vector<Coordinate> normals;
	QVector<float> slicesWidths;
	float minModelX, minModelY, minModelZ, maxModelX, maxModelY, maxModelZ;
	float knifeSize = 10.0;
	int cutAxis; // 0 - x. 1 - y.

	float moveX = 0.0, moveY = 0.0, moveZ = 0.0,
		  rotX = 0.0, rotY = 0.0, rotZ = 0.0,
		  scale = 0.2,
		  pressMoveX, pressMoveY, pressMoveZ;

protected:
	void mousePressEvent(QMouseEvent* event);
	void mouseReleaseEvent(QMouseEvent* event);
	void mouseMoveEvent(QMouseEvent* event);
	void wheelEvent(QWheelEvent* event);

private:
//	int getObjectIndex(const QPoint &pos);
	void draw();
	QColor backgroundColor, modelColor, knifeXColor, knifeYColor;
	QPoint mousePos, mousePressPos;
};
#endif // CANVAS_H
