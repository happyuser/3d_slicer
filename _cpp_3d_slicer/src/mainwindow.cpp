﻿#include "ui_mainwindow.h"
#include "mainwindow.h"
#include "calculatevolumeworker.h"
#include "cutmodelworker.h"
#include "finishdialog.h"
#include "progressdialog.h"
#include <QPicture>
#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
#include <QProcess>
#include <QThread>
#include <cmath>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
		QMainWindow(parent),
		ui(new Ui::MainWindow),
		slicesNumbersLabels(),
		widthsEdits(),
		fixedChecks(),
		modelSize(),
		savedFilesNames(),
		settings(),
		sliceVolume(),
		slicesCutPoints(),
		slicesWidths(),
		colors(),
		table2_slicesNumbersLabels(),
		table2_minusButtons(),
		table2_plusButtons(),
		table2_widthsEdits(),
		table2_volumesEdits(),
		table2_cutPointsEdits(),
		fixSlices() {
	table2_horizontalLayout = NULL;
	ui->setupUi(this);
	setWindowTitle(QApplication::applicationName());
	connect(ui->addKnifeObjectCheck, SIGNAL(toggled(bool)),
			ui->knifeObjectWidget, SLOT(setVisible(bool)));

	loadSettings();
	setStyle();

	canvas = new Canvas(this, colors);
	canvas->setMinimumWidth(750);
	canvas->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	ui->horizontalLayout->addWidget(canvas);

	if (settings.contains("bannerPicture")) {
		QString pictureFilePath(QApplication::applicationDirPath() + "/" +
								settings["bannerPicture"]);
		QPixmap pixmap;
		if (pixmap.load(pictureFilePath))
			ui->bannerPicture->setPixmap(pixmap.scaledToHeight(60));
		else showWarning(
				"Could not load \"" + QDir::toNativeSeparators(pictureFilePath) +
				"\" picture.");
	} else ui->bannerPicture->setVisible(false);
	if (settings.contains("defualtSlices"))
		ui->numberOfSlicesEdit->setText(settings["defualtSlices"]);
	// ui->numberOfSlicesEdit->setText("2"); // Для тесту.

	if (settings.contains("defualtCutAxis")) {
		int i;
		if ((i = ui->cutAxisCombo->findText(settings["defualtCutAxis"])) != -1)
			ui->cutAxisCombo->setCurrentIndex(i);
	}

    createWidthsTable();

//	ui->modelFilePathEdit->setText(
//			QDir::toNativeSeparators(QApplication::applicationDirPath() + "/torus.stl"));
	ui->modelFilePathEdit->setText(
			QDir::toNativeSeparators(QApplication::applicationDirPath() +
			"/20-30-50-hollow_ks_fix.stl"));

	ui->knifeObjectEdit->setText(QDir::toNativeSeparators(
			QApplication::applicationDirPath() + "/cube_wave.stl"));
	if (settings.contains("use_custom_slice_object")) {
		ui->addKnifeObjectCheck->setChecked(settings["use_custom_slice_object"] == "1");
		ui->knifeObjectWidget->setVisible(settings["use_custom_slice_object"] == "1");
	}

	if (getModelSizeAndType()) {
		calculateSlicesWidths();
		loadModelToPreview();
	} else showWarning("Can`t calculate model size.");
}

MainWindow::~MainWindow() {
	delete ui;
}

void MainWindow::setStyle() {
	// QComboBox { color: black; background-color: white }
	ui->centralWidget->setStyleSheet("\
			QLineEdit, QPushButton { color: black; background-color: white }\
			QPushButton { font-weight: bold; \
				padding-top: 3px; padding-bottom: 3px; \
				padding-left: 10px; padding-right: 10px }\
			#centralWidget {\
				background-color: " + getColor("basicBackground", "#7F7F7F") + " }\
			#container1 QLabel, #container2 QLabel { color: white }\
			#container3 QLabel, #container4 QLabel, #container5 QLabel { color: black }\
			#container4 QLineEdit, #container5 QLineEdit {\
				border-width: 1px; border-style: solid; border-color: #C1C1C3 }\
			#container1 { background-color: " + getColor("strip1", "#0071C0") + " }\
			#container2 { background-color: " + getColor("strip2", "#558ED5") + " }\
			#container3 { background-color: " + getColor("strip3", "#D9D9D9") + " }\
			#container4 { background-color: " + getColor("strip4", "#F0F0F0") + " }\
			#container5 { background-color: " + getColor("strip5", "#F0F0F0") + " }\
			#container6 { background-color: " + getColor("strip6", "#DA9695") + " }\
			#container7 { background-color: " + getColor("strip7", "#C00000") + " }\
			#calculateCutPointslabel { font: bold 19px; color: white; \
				margin-top: 7px; margin-bottom: 7px }\
			#container6 QCheckBox { border: none; color: black }\
			#createArrayButton {\
				margin-top: 10px; margin-bottom: 10px; \
				padding-left: 20px; padding-right: 20px }\
			#splitModelButton { padding-left: 20px; padding-right: 20px }");
}

void MainWindow::loadSettings() {
	QString filePath(QApplication::applicationDirPath() + "/settings.ini"), section(""),
			s;
	int i;
	QFile file(filePath);

	if (file.open(QFile::ReadOnly | QFile::Text) == false) {
		showWarning("Open error of \"" + QDir::toNativeSeparators(filePath) +
					"\" file for read.");
		return;
	}

	while (file.atEnd() == false) {
		s = QString::fromUtf8(file.readLine()).trimmed();
		if (s.isEmpty() || s.startsWith(';')) continue;

		if (s == "[colors]") {
			section = "[colors]";
			continue;
		}

		i = s.indexOf('=');
		if (i == -1) continue;

		if (section == "[colors]") {
			colors[s.left(i)] = s.mid(i + 1);
			continue;
		}

		if (s.left(i) == "Fix_slice") { // Condition for loading fixed slices.
			s = s.mid(i + 1);
			if ((i = s.indexOf(',')) != -1)
				fixSlices[s.left(i).toInt() - 1] = s.mid(i + 1).toDouble();
		} else settings[s.left(i)] = s.mid(i + 1);
	}

	if (!colors.contains("previewModelBackground"))
		colors["previewModelBackground"] = "white";
	if (!colors.contains("modelColor")) colors["modelColor"] = "#646464";
	if (!colors.contains("knifeX")) colors["knifeX"] = "red";
	if (!colors.contains("knifeY")) colors["knifeY"] = "green";

	file.close();
}

QString MainWindow::getColor(const QString &element, const QString &defaultValue) {
	return (colors.contains(element) ? colors[element] : defaultValue);
}

// The createWidthsTable function dynamicaly creates the table with slices numbers,
// widths editors and checkboxes.
void MainWindow::createWidthsTable() {
	QLabel *label; QLineEdit *edit; QCheckBox *check; int i;
	for (i = 0; i < ui->numberOfSlicesEdit->text().toInt(); i++) {
		label = new QLabel(QString::number(i + 1));
		ui->widthsGridLayout->addWidget(label, 0, i + 1);
		slicesNumbersLabels += label;

		edit = new QLineEdit;
		ui->widthsGridLayout->addWidget(edit, 1, i + 1);
		widthsEdits += edit;
		connect(edit, SIGNAL(textEdited(const QString &)),
				this, SLOT(widthEdit_onTextEdited(const QString &)));

		check = new QCheckBox;
		ui->widthsGridLayout->addWidget(check, 2, i + 1);
		fixedChecks += check;
	}

	for (i = 0; i < slicesNumbersLabels.count(); i++) {
		if (fixSlices[i] != 0) {
			fixedChecks[i]->setCheckState(Qt::Checked);
			widthsEdits[i]->setText(QString::number(fixSlices[i]));
		}
	}
}

bool MainWindow::getModelSizeAndType() { // Returns false on error.
	QStringList parameters; QProcess process; int r;
	QString standardOutput, standardError;

	ui->modelSizeXEdit->setText("");
	ui->modelSizeYEdit->setText("");
	ui->modelSizeZEdit->setText("");

	if (!settings.contains("admeshExecutableFilePath")) {
		showWarning("There is no admeshExecutableFilePath in the settings.");
		return false;
	}

    parameters.append(ui->modelFilePathEdit->text());
    process.start(settings["admeshExecutableFilePath"], parameters);
    process.waitForFinished(-1);
    r = ((process.exitStatus() == QProcess::NormalExit) ? process.exitCode() : -1);
    standardOutput = process.readAllStandardOutput(),
	standardError = process.readAllStandardError();
    if (r) {
		showWarning("admesh exit code: " + QString::number(r) +
					"\nadmesh result:\n" + standardOutput + standardError);
		return false;
	}

	standardOutput.remove('\r');

	QRegExp rx(
		"\n=+ Size =+\n"
		"Min X = ([-.\\d ]+), Max X = ([-.\\d ]+)\n"
		"Min Y = ([-.\\d ]+), Max Y = ([-.\\d ]+)\n"
        "Min Z = ([-.\\d ]+), Max Z = ([-.\\d ]+)\n"
        ".*Volume   :  ([-.\\d ]*)");
	if (rx.indexIn(standardOutput) == -1) {
		QMessageBox::warning(this, QApplication::applicationName(),
							 "Could not parse the admesh output.\n" + standardOutput);
		return false;
	}

    //calculating general volume
    fullVolume = rx.cap(7).trimmed().toDouble();

    //calculating other
    modelSize["minX"] = rx.cap(1).trimmed().toFloat();
    modelSize["maxX"] = rx.cap(2).trimmed().toFloat();
    modelSize["minY"] = rx.cap(3).trimmed().toFloat();
    modelSize["maxY"] = rx.cap(4).trimmed().toFloat();
    modelSize["minZ"] = rx.cap(5).trimmed().toFloat();
    modelSize["maxZ"] = rx.cap(6).trimmed().toFloat();
    ui->modelSizeXEdit->setText(QString::number(modelSize["maxX"] - modelSize["minX"]));
    ui->modelSizeYEdit->setText(QString::number(modelSize["maxY"] - modelSize["minY"]));
    ui->modelSizeZEdit->setText(QString::number(modelSize["maxZ"] - modelSize["minZ"]));

	isStlModelBinary = (
		QRegExp("File type +: Binary STL file\n").indexIn(standardOutput) != -1);
/*
	QStringList list; QMapIterator<QString, float> iterator(modelSize);
	while (iterator.hasNext()) {
		iterator.next();
		list.append(iterator.key() + "=" + QString::number(iterator.value()));
	}
	QMessageBox::information(this, QApplication::applicationName(), list.join("\n"));
*/
	return true;
}

void MainWindow::widthEdit_onTextEdited(const QString& text) {
	QObject *s = sender(); int i;
	if (s == NULL) return;
	if ((i = widthsEdits.indexOf((QLineEdit*)s)) != -1) {

		// Turn the checkbox on when user change the slice width.
		fixedChecks[i]->setCheckState(Qt::Checked);

		if ((!text.isEmpty()) && (i < canvas->slicesWidths.count())) {
			canvas->slicesWidths[i] = text.toFloat();
			canvas->updateGL();
		}
	}
}

void MainWindow::on_browseButton_clicked() {
	sliceVolume.clear();
    fullVolume = 0;
	QString filePath(QFileDialog::getOpenFileName(
			this, "Select stl file", "", "stl files (*.stl);;All files (*.*)"));
	if (!filePath.isEmpty()) {
		ui->modelFilePathEdit->setText(QDir::toNativeSeparators(filePath));
		qApp->processEvents();
		if (!getModelSizeAndType()) {
			showWarning("Can`t calculate model size.");
			return;
		}
		calculateSlicesWidths();
		loadModelToPreview();
		canvas->updateGL();
	}
}

void MainWindow::on_cutAxisCombo_currentIndexChanged(int index) {
    on_calculateButton_clicked();
	canvas->cutAxis = index;
	canvas->updateGL();
}

void MainWindow::on_createArrayButton_clicked() {
	// Remove old elements from widhts table:
	for (int i = 0; i < slicesNumbersLabels.count(); i++) {
		ui->widthsGridLayout->removeWidget(slicesNumbersLabels[i]);
		delete slicesNumbersLabels[i];
		ui->widthsGridLayout->removeWidget(widthsEdits[i]);
		delete widthsEdits[i];
		ui->widthsGridLayout->removeWidget(fixedChecks[i]);
		delete fixedChecks[i];
	}
	slicesNumbersLabels.clear();
	widthsEdits.clear();
	fixedChecks.clear();

	createWidthsTable();
    on_calculateButton_clicked();
	canvas->slicesWidths.clear();
	for (QLineEdit* w : widthsEdits) canvas->slicesWidths += w->text().toFloat();
	canvas->updateGL();
}

/*
void MainWindow::on_testButton_clicked() {
	if (!QDir::setCurrent(QApplication::applicationDirPath())) return;

	QDir dir(QApplication::applicationDirPath());
	if (!dir.exists("scad_output")) {
		if (!dir.mkdir("scad_output")) {
			QMessageBox::warning(this, QApplication::applicationName(),
				"Could not make " +
				QDir::toNativeSeparators(QApplication::applicationDirPath() +
					"/scad_output") + " directory.");
			return;
		}
	}

	QStringList parameters, a; QProcess process; int i, r;
	QString standardOutput, standardError;

//	ui->testButton->setEnabled(false);
	qApp->processEvents();
	for (i = 1; i <= 6; i++) {
		parameters.clear();
		parameters.append("-o");
        parameters.append(QString("scad_output") + QDir::separator() + "r" +
						  QString::number(i) + ".stl");
		parameters.append("-D user_file=\"torus.stl\"");
		parameters.append("-D p_s_slice_size=\"0,159,200,650,800\"");
		parameters.append("-D p=\"" + QString::number(i) + "\"");
		parameters.append("slicer.scad");

		process.start(settings["openscadExecutableFilePath"], parameters);
		process.waitForFinished(-1); // will wait forever until finished
		r = ((process.exitStatus() == QProcess::NormalExit) ? process.exitCode() : -1);
		standardOutput = process.readAllStandardOutput(),
		standardError = process.readAllStandardError();

		QMessageBox::information(this, QApplication::applicationName(),
"openscad parameters:\n" + parameters.join("\n") +
"\n-----------------------------------------------------------\nopenscad exit code: " +
QString::number(r) + "\nopenscad result:\n" + standardOutput + standardError);
	}
//	ui->testButton->setEnabled(true);
}
*/


void MainWindow::calculateSlicesWidths() {
    double modelSize, fixedWidthsSum = 0;
    int i, n = 0; // n is a number of chackboxes that are turned on.
    if (ui->modelSizeXEdit->text().isEmpty()) return;
    modelSize = ((ui->cutAxisCombo->currentText() == "x") ?
        ui->modelSizeXEdit->text().toDouble() : ui->modelSizeYEdit->text().toDouble());

    for (i = 0; i < fixedChecks.count(); i++) {
        if (fixedChecks[i]->checkState() == Qt::Checked) {
            n++;
            fixedWidthsSum += widthsEdits[i]->text().toDouble();
        }
    }

    for (i = 0; i < widthsEdits.count(); i++) {
        if (n) {
            if (fixedChecks[i]->checkState() == Qt::Unchecked)
                widthsEdits[i]->setText(QString::number(
                    (modelSize - fixedWidthsSum) / (widthsEdits.count() - n)));
        } else
            widthsEdits[i]->setText(QString::number(modelSize / widthsEdits.count()));
    }
}

void MainWindow::calculateSlicersWidthsFromCutPoints() {
	for (QLineEdit* w : table2_cutPointsEdits) if (w->text().isEmpty()) return;

	QString s;
	for (int i = 1; i < table2_cutPointsEdits.count(); i++) {
		s = QString::number(table2_cutPointsEdits[i]->text().toDouble() -
				table2_cutPointsEdits[i - 1]->text().toDouble());
		widthsEdits[i]->setText(s);
		table2_widthsEdits[i]->setText(s);
	}

	double modelSize = ((ui->cutAxisCombo->currentText() == "x") ?
		ui->modelSizeXEdit->text().toDouble() : ui->modelSizeYEdit->text().toDouble());
	widthsEdits.first()->setText(table2_cutPointsEdits.first()->text());
	table2_widthsEdits.first()->setText(table2_cutPointsEdits.first()->text());
	s = QString::number(modelSize - table2_cutPointsEdits.last()->text().toDouble());
	widthsEdits.last()->setText(s);
	table2_widthsEdits.last()->setText(s);

	canvas->slicesWidths.clear();
	for (QLineEdit* w : widthsEdits) canvas->slicesWidths += w->text().toFloat();
	canvas->updateGL();
}

void MainWindow::on_calculateButton_clicked() {
	calculateSlicesWidths();
	canvas->slicesWidths.clear();
	for (QLineEdit* w : widthsEdits) canvas->slicesWidths += w->text().toFloat();
	canvas->updateGL();
}

void MainWindow::onCutModelSuccess() {
	FinishDialog(this, savedFilesNames).exec();
}

void MainWindow::on_splitModelButton_clicked() {
    if (!settings.contains("openscadExecutableFilePath")) {
		showWarning("There is no openscadExecutableFilePath in the settings.");
        return;
    }
    if (ui->modelFilePathEdit->text().isEmpty()) {
		showWarning("The model file path is not given.");
        return;
    }
	if (ui->addKnifeObjectCheck->isChecked() &&
			ui->knifeObjectEdit->text().isEmpty()) {
		showWarning("The file path of knife object is not given.");
		return;
	}

	isOperationCanceled = false;
	setButtonsEnabledState(false);
	ProgressDialog* dialog = new ProgressDialog(this);
	dialog->show();

	savedFilesNames.clear();
	QThread* thread = new QThread;
	CutModelWorker* worker = new CutModelWorker(this);
	worker->moveToThread(thread);
	connect(worker, SIGNAL(progressChanged(int)), dialog, SLOT(setProgress(int)));
	connect(worker, SIGNAL(success()), this, SLOT(onCutModelSuccess()));
	connect(worker, SIGNAL(success()), dialog, SLOT(accept()));
	connect(worker, SIGNAL(error(QString)), dialog, SLOT(accept()));
	connect(worker, SIGNAL(error(QString)), this, SLOT(showWarning(QString)));
	connect(thread, SIGNAL(started()), worker, SLOT(process()));
	connect(worker, SIGNAL(finished()), this, SLOT(enableButtons()));
	connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
	connect(worker, SIGNAL(finished()), dialog, SLOT(deleteLater()));
	connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
	connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
	thread->start();
/*
    QProcess::startDetached("openscad");
    QString s("cd D:/3d_slicer/scad /n openscad -o ../scad_output/r%G.stl -D \"user_file=\\\"torus.stl\\\"\" -D \"p_s_slice_size=\\\"0,159,200,650,800\\\"\" -D \"p=\\\"%G\\\"\" slicer.scad");
    const char[] s = "cd D:/3d_slicer/scad";
    system(s);
*/
}

void MainWindow::showWarning(QString message) {
	QMessageBox::warning(this, qAppName(), message);
}

void MainWindow::setButtonsEnabledState(bool isEnabled) {
	ui->cutAxisCombo->setEnabled(isEnabled);
	ui->browseButton->setEnabled(isEnabled);
	ui->browseForKnifeObjectButton->setEnabled(isEnabled);
	ui->createArrayButton->setEnabled(isEnabled);
	ui->splitModelButton->setEnabled(isEnabled);
	ui->calculateButton->setEnabled(isEnabled);
	ui->calculateVolumeButton->setEnabled(isEnabled);
}

void MainWindow::enableButtons() { setButtonsEnabledState(true); }

void MainWindow::cutPointEdit_onTextEdited(const QString& /* text */) {
	calculateSlicersWidthsFromCutPoints();
}

void MainWindow::onMinusButtonClicked() {
	QObject *s = sender(); int i;
	if (s == NULL) return;
	if ((i = table2_minusButtons.indexOf((QPushButton*)s)) != -1) {
		double d = table2_cutPointsEdits[i]->text().toDouble();
		table2_cutPointsEdits[i]->setText(QString::number(d - 1));
		calculateSlicersWidthsFromCutPoints();
	}
}

void MainWindow::onPlusButtonClicked() {
	QObject *s = sender(); int i;
	if (s == NULL) return;
	if ((i = table2_plusButtons.indexOf((QPushButton*)s)) != -1) {
		double d = table2_cutPointsEdits[i]->text().toDouble();
		table2_cutPointsEdits[i]->setText(QString::number(d + 1));
		calculateSlicersWidthsFromCutPoints();
	}
}

void MainWindow::onCalculateVolumeReady() {
//	std::cout << slicesWidths.count() << std::endl;
	table2_horizontalLayout = new QHBoxLayout;
	QLabel *label; QPushButton *minusButton, *plusButton; QLineEdit *edit;
	QString style;
	int i;
	canvas->slicesWidths.clear();
	table2_horizontalLayout->addStretch();
	for (i = 0; i < slicesWidths.count(); i++) {

		label = new QLabel(QString::number(i + 1));
		ui->resultGridLayout->addWidget(label, 0, i + 1);
		table2_slicesNumbersLabels += label;

		widthsEdits[i]->setText(QString::number(slicesWidths[i]));
		canvas->slicesWidths += slicesWidths[i];

		edit = new QLineEdit(QString::number(slicesWidths[i]));
		edit->setReadOnly(true);
		ui->resultGridLayout->addWidget(edit, 1, i + 1);
		table2_widthsEdits += edit;

		edit = new QLineEdit(QString::number(sliceVolume[i], 'f'));
		edit->setReadOnly(true);
		ui->resultGridLayout->addWidget(edit, 2, i + 1);
		table2_volumesEdits += edit;

		if (i < (slicesWidths.count() - 1)) {
			minusButton = new QPushButton("-");
			plusButton = new QPushButton("+");
			style = "width: " +
QString::number(plusButton->fontMetrics().boundingRect("+").width() + 8) +
"; padding-top: 1px; padding-bottom: 1px; \
padding-left: 1px; padding-right: 1px; border-style: none; font-weight: normal; \
color: black; background-color: #DADADA";
			minusButton->setStyleSheet(style);
			plusButton->setStyleSheet(style);
//			width = plusButton->fontMetrics().boundingRect("+").width() + 7;
//			minusButton->setMaximumWidth(width);
//			plusButton->setMaximumWidth(width);

			edit = new QLineEdit(QString::number(slicesCutPoints[i]));
			edit->setMaximumWidth(
					edit->fontMetrics().boundingRect("99999999").width() + 7);

			table2_horizontalLayout->addWidget(minusButton);
			table2_horizontalLayout->addWidget(edit);
			table2_horizontalLayout->addWidget(plusButton);
			table2_horizontalLayout->addStretch();
			table2_minusButtons += minusButton;
			table2_plusButtons += plusButton;
			table2_cutPointsEdits += edit;

			connect(minusButton, SIGNAL(clicked()), this, SLOT(onMinusButtonClicked()));
			connect(plusButton, SIGNAL(clicked()), this, SLOT(onPlusButtonClicked()));
			connect(edit, SIGNAL(textEdited(const QString &)),
					this, SLOT(cutPointEdit_onTextEdited(const QString &)));
		}
	}
	ui->resultGridLayout->addLayout(table2_horizontalLayout, 3, 1, 1, -1);
	canvas->updateGL();
}

void MainWindow::on_calculateVolumeButton_clicked() {
	QString logFilePath(QApplication::applicationDirPath() + "/log.txt");
	if (QFile::exists(logFilePath)) {
		if (!QFile::remove(logFilePath)) {
			showWarning("Could not remove the " +
						QDir::toNativeSeparators(logFilePath) + " file.");
			return;
		}
	}

	// Remove old elements from table 2.
	for (int i = 0; i < table2_slicesNumbersLabels.count(); i++) {
		ui->resultGridLayout->removeWidget(table2_slicesNumbersLabels[i]);
		delete table2_slicesNumbersLabels[i];
		ui->resultGridLayout->removeWidget(table2_widthsEdits[i]);
		delete table2_widthsEdits[i];
		ui->resultGridLayout->removeWidget(table2_volumesEdits[i]);
		delete table2_volumesEdits[i];
	}
	if (table2_horizontalLayout) {
		for (int i = 0; i < table2_cutPointsEdits.count(); i++) {
			table2_horizontalLayout->removeWidget(table2_minusButtons[i]);
			table2_horizontalLayout->removeWidget(table2_plusButtons[i]);
			table2_horizontalLayout->removeWidget(table2_cutPointsEdits[i]);
			delete table2_plusButtons[i];
			delete table2_minusButtons[i];
			delete table2_cutPointsEdits[i];
		}
		ui->resultGridLayout->removeItem(table2_horizontalLayout);
		delete table2_horizontalLayout;
		table2_minusButtons.clear();
		table2_plusButtons.clear();
		table2_cutPointsEdits.clear();
	}
	table2_slicesNumbersLabels.clear();
	table2_widthsEdits.clear();
	table2_volumesEdits.clear();

	isOperationCanceled = false;
	setButtonsEnabledState(false);
	ProgressDialog* dialog = new ProgressDialog(this, false);
	dialog->show();

	QThread* thread = new QThread;
	CalculateVolumeWorker* worker = new CalculateVolumeWorker(this);
	worker->moveToThread(thread);
	connect(worker, SIGNAL(statusChanged(QString)), dialog, SLOT(setStatus(QString)));
	connect(worker, SIGNAL(progressChanged(int)), dialog, SLOT(setProgress(int)));
	connect(worker, SIGNAL(progress2Changed(int)), dialog, SLOT(setProgress2(int)));
	connect(worker, SIGNAL(error(QString)), dialog, SLOT(hide()));
	connect(worker, SIGNAL(error(QString)), this, SLOT(showWarning(QString)));
	connect(thread, SIGNAL(started()), worker, SLOT(process()));
	connect(worker, SIGNAL(ready()), this, SLOT(onCalculateVolumeReady()));
	connect(worker, SIGNAL(finished()), dialog, SLOT(accept()));
	connect(worker, SIGNAL(finished()), this, SLOT(enableButtons()));
	connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
	connect(worker, SIGNAL(finished()), dialog, SLOT(deleteLater()));
	connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
	connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
	thread->start();
} // Кінець функції on_calculateVolumeButton_clicked.

void MainWindow::on_browseForKnifeObjectButton_clicked() {
	QString filePath(QFileDialog::getOpenFileName(
			this, "Select stl file", "", "stl files (*.stl);;All files (*.*)"));
	if (!filePath.isEmpty())
		ui->knifeObjectEdit->setText(QDir::toNativeSeparators(filePath));
}

void MainWindow::writeToLog(const QString &s) {
	QString filePath(QApplication::applicationDirPath() + "/log.txt");
	QFile file(filePath);
	if (!file.open(QFile::Append)) {
		showWarning("Error of open the \"" +
				   QDir::toNativeSeparators(filePath) + "\" file for write.");
		return;
	}
	file.write(s.toUtf8());
	file.close();
}

void MainWindow::on_previewButton_clicked() {
	loadModelToPreview();
	canvas->updateGL();
}


// true - success.
bool MainWindow::convertBinaryStlFileToAscii(QString& outputFilePath) {
	QString outputDirectoryPath(QApplication::applicationDirPath() + "/tmp");

	// Create outputDirectoryPath if it does not exist.
	if (!QDir(outputDirectoryPath).exists()) {
		if (!QDir(QApplication::applicationDirPath()).mkdir("tmp")) {
			showWarning("Could not make the " +
						QDir::toNativeSeparators(outputDirectoryPath) +
						" directory.");
			return false;
		}
	}

	// Set outputDirectoryPath as current directory.
	if (!QDir::setCurrent(outputDirectoryPath)) {
		showWarning("Could not set the " + QDir::toNativeSeparators(outputDirectoryPath) +
					" directory as current.");
		return false;
	}

	QString modelFileName(QFileInfo(ui->modelFilePathEdit->text()).fileName());

	if (QFile::exists(modelFileName)) {
		if (!QFile::remove(modelFileName)) {
			showWarning("Could not remove the " +
					QDir::toNativeSeparators(outputDirectoryPath + "/" + modelFileName) +
					" file.");
			return false;
		}
	}

	QStringList parameters; QProcess process; int r;
	QString standardOutput, standardError;

	parameters.append("-a");
	parameters.append(modelFileName);
	parameters.append(ui->modelFilePathEdit->text());

	process.start(settings["admeshExecutableFilePath"], parameters);
	process.waitForFinished(-1);
	r = ((process.exitStatus() == QProcess::NormalExit) ? process.exitCode() : -1);
	standardOutput = process.readAllStandardOutput(),
	standardError = process.readAllStandardError();
	if (r) {
		showWarning("admesh exit code: " + QString::number(r) +
					"\nadmesh result:\n" + standardOutput + standardError);
		return false;
	}
	outputFilePath = outputDirectoryPath + "/" + modelFileName;
	return true;
}

void MainWindow::loadModelToPreview() {
	canvas->vertex.clear();
	canvas->normals.clear();
	canvas->stl.rows.clear();
	canvas->stl.vertex.clear();
	canvas->stl.normal.clear();
	canvas->slicesWidths.clear();
	QString stlFilePath;
	if (isStlModelBinary) {
		if (!convertBinaryStlFileToAscii(stlFilePath)) return;
	} else stlFilePath = ui->modelFilePathEdit->text();
	canvas->stl.loadFile(QDir::toNativeSeparators(stlFilePath).toUtf8());
	canvas->stl.parse();
	canvas->vertex = canvas->stl.getVertex();
	canvas->normals = canvas->stl.getNormals();
	canvas->cutAxis = ui->cutAxisCombo->currentIndex();
	canvas->minModelX = modelSize["minX"];
	canvas->minModelY = modelSize["minY"];
	canvas->minModelZ = modelSize["minZ"];
	canvas->maxModelX = modelSize["maxX"];
	canvas->maxModelY = modelSize["maxY"];
	canvas->maxModelZ = modelSize["maxZ"];

	canvas->moveX = 0.0;
	canvas->moveY = 0.0;
	canvas->moveZ = 0.0;
	canvas->rotX = 310.0;
	canvas->rotY = 0.0;
	canvas->rotZ = 45.0;

	for (QLineEdit* w : widthsEdits) canvas->slicesWidths += w->text().toFloat();

	// Вибір масштабу.
	float m = 0.0, f; QMapIterator<QString, float> iterator(modelSize);
	while (iterator.hasNext()) {
		iterator.next();
		f = fabs(iterator.value());
//		std::cout << iterator.value() << " : " << f << std::endl;
		if (f > m) m = f;
	}
	canvas->scale = 2.8 / m;

	// Вибір розміру ножа.
	const char* a[] = { "X", "Y", "Z" };
	m = 0.0;
	for (const char* s : a) {
		f = modelSize[QString("max") + s] - modelSize[QString("min") + s];
		if (f > m) m = f;
	}
	canvas->knifeSize = m * 1.3;
}
