QT += core gui opengl widgets
#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = 3dSlicer
TEMPLATE = app
unix: LIBS += -lGLU

# Bump optimization up to -O3 in release builds
#QMAKE_CXXFLAGS_RELEASE -= -O2
#QMAKE_CXXFLAGS_RELEASE += -O3

#CONFIG += c++11
#CONFIG += console

SOURCES += \
    src/canvas.cpp \
    src/coordinate.cpp \
    src/stlobject.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/calculatevolumeworker.cpp \
    src/cutmodelworker.cpp \
    src/progressdialog.cpp \
    src/finishdialog.cpp
HEADERS += \
    src/canvas.h \
    src/coordinate.h \
    src/stlobject.h \
    src/mainwindow.h \
    src/calculatevolumeworker.h \
    src/cutmodelworker.h \
    src/progressdialog.h \
    src/finishdialog.h
FORMS += mainwindow.ui
RESOURCES += 3dslicer.qrc

win32 {
    CONFIG += static
    RC_FILE = 3dslicer.rc
}
